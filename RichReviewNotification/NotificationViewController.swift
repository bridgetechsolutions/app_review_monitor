//
//  NotificationViewController.swift
//  RichReviewNotification
//
//  Created by Mark Bridges on 18/12/2016.
//  Copyright © 2016 Mark Bridges. All rights reserved.
//

import UIKit
import UserNotifications
import UserNotificationsUI

class NotificationViewController: UIViewController, UNNotificationContentExtension {
    
    
    @IBOutlet weak var contentView: UIView!
    
    lazy var reviewView: ReviewView = {
        let reviewView = Bundle.main.loadNibNamed("ReviewView", owner: self, options: nil)?[0] as! ReviewView
        reviewView.translatesAutoresizingMaskIntoConstraints = false
        return reviewView
    } ()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        contentView.addSubview(reviewView)
        reviewView.autoPinEdgesToSuperviewMargins()
    }
    
    func didReceive(_ notification: UNNotification) {
        
        let reviewBuilder = ReviewBuilder()
        let review = reviewBuilder.newReviewConstructedFrom(notification: notification)
        
        reviewView.review = review
    }
    
}
