//
//  HTTPClientMock.swift
//  App Review Monitor
//
//  Created by Mark Bridges on 12/12/2016.
//  Copyright © 2016 Mark Bridges. All rights reserved.
//

import Foundation
@testable import App_Review_Monitor


class HTTPClientMock: HTTPClient {
    
    var urlOfLastRequest: URL? {
        return allUrlsCalled.last
    }
    var mockedResults: [URL:HTTPClientResult]?
    var allUrlsCalled: [URL] = []
    
    override func makeNetworkRequest(with url: URL, completion: @escaping ((HTTPClientResult) -> Void)) -> URLSessionDataTask {
        
        allUrlsCalled.append(url)
        
        guard
            let mockedResults = mockedResults,
            let mockedResult = mockedResults[url] else {
            fatalError("HTTPClientMock called at \(url) without a mocked result to return")
        }
        
        completion(mockedResult)
        
        return URLSessionDataTask()
    }
}
