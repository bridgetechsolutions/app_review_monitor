//
//  FetchReviewsForSelectedCountriesOperationTests.swift
//  App Review Monitor
//
//  Created by Mark Bridges on 12/12/2016.
//  Copyright © 2016 Mark Bridges. All rights reserved.
//

import XCTest
@testable import App_Review_Monitor


class FetchReviewsForSelectedCountriesOperationTests: XCTestCase {
    
    
    func testFetchReviewsCreatesReviewsFromAllCountries() {
        
        let bundle = Bundle(for: FetchReviewsFromAppStoreOperationTests.self)
        let xmlFixtureUrl1 = bundle.url(forResource: "singlepagereviews", withExtension: "xml")!
        let data1 = try! Data(contentsOf: xmlFixtureUrl1)
        let xmlFixtureUrl2 = bundle.url(forResource: "singlepagereviews2", withExtension: "xml")!
        let data2 = try! Data(contentsOf: xmlFixtureUrl2)
        
        let expectedURL1 = URL(string:"https://itunes.apple.com/gb/rss/customerreviews/page=1/id=1/sortby=mostrecent/xml")!
        let expectedURL2 = URL(string:"https://itunes.apple.com/us/rss/customerreviews/page=1/id=1/sortby=mostrecent/xml")!
        
        let httpClientMock = HTTPClientMock()
        httpClientMock.mockedResults = [expectedURL1 : HTTPClient.HTTPClientResult.success(data: data1),
                                        expectedURL2 : HTTPClient.HTTPClientResult.success(data: data2)]
        
        
        let countries: Set<Country> = [Country(id: "gb", name: ""), Country(id: "us", name: "")]
        
        let app = App(id: "1", name: "A", iconImageURL: URL(string: "http://www.anywhere.com")!, store: .ios, countries: countries, subscriptionID: "123", lastReviewsUpdate: Date())
        
        let operation = FetchReviewsForSelectedCountriesOperation(app: app, httpClient: httpClientMock)
        
        let myExpectation = expectation(description: "Call should complete")
        
        operation.completionBlock = {
            switch operation.operationStatus {
                
            case .succeeded(let result):
                let reviews = result as! Set<FullReview>
                XCTAssertEqual(reviews.count, 9)
                
            default:
                XCTFail("Call with valid response failed")
            }
            
            myExpectation.fulfill()
        }
        
        operation.start()
        
        waitForExpectations(timeout: ExpectationWaitTime, handler: nil)
    }
    
}
