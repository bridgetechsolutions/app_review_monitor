//
//  StoreSearchServiceTests.swift
//  App Review Monitor
//
//  Created by Mark Bridges on 12/12/2016.
//  Copyright © 2016 Mark Bridges. All rights reserved.
//

import XCTest
@testable import App_Review_Monitor


class StoreSearchServiceTests: XCTestCase {
    
    
    func testCallingSearchForMacAppsFormsTheCorrectURL() {
        
        let expectedURL = URL(string:"https://itunes.apple.com/gb/search?term=asset%20catalog%20creator&entity=macSoftware&media=software")!
        
        let mockClient = HTTPClientMock()
        mockClient.mockedResults = [expectedURL : .success(data: Data())]
        
        let searchService = StoreSearchService(httpClient: mockClient)
        searchService.getApps(forSearchTerm: "asset catalog creator", countryId: "gb", store: .mac) { result in
            
        }
        
        XCTAssertEqual(mockClient.urlOfLastRequest, expectedURL)
    }
    
    
    func testCallingSearchReturnsApps() {
        
        let expectedURL = URL(string:"https://itunes.apple.com/gb/search?term=asset%20catalog%20creator&entity=macSoftware&media=software")!
        
        let bundle = Bundle(for: FetchReviewsFromAppStoreOperationTests.self)
        let jsonFixtureUrl = bundle.url(forResource: "search", withExtension: "json")!
        let data = try! Data(contentsOf: jsonFixtureUrl)
        
        let mockClient = HTTPClientMock()
        mockClient.mockedResults = [expectedURL : .success(data: data)]
        
        let searchService = StoreSearchService(httpClient: mockClient)
        
        let myExpectation = expectation(description: "Call should complete")
        
        searchService.getApps(forSearchTerm: "asset catalog creator", countryId: "gb", store: .mac) { result in
            
            switch result {
            case .success(let apps):
                XCTAssertEqual(apps.count, 13)
                
                let firstApp = apps.first!
                
                XCTAssertEqual(firstApp.id, "866571115")
                XCTAssertEqual(firstApp.name, "Asset Catalog Creator Free")
                XCTAssertEqual(firstApp.iconImageURL, URL(string: "http://is1.mzstatic.com/image/thumb/Purple71/v4/f9/00/dd/f900dddc-ecac-1224-c2b2-2486103fdd71/source/512x512bb.png"))
                XCTAssertEqual(firstApp.store, .mac)
                
            default:
                XCTFail("Call should have succeeded")
                
            }
            
            myExpectation.fulfill()
        }
        
        waitForExpectations(timeout: ExpectationWaitTime, handler: nil)
        
    }

}
