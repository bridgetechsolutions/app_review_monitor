//
//  App+Testing.swift
//  App Review Monitor
//
//  Created by Mark Bridges on 10/12/2016.
//  Copyright © 2016 Mark Bridges. All rights reserved.
//

import Foundation
import CoreData
@testable import App_Review_Monitor

let ExpectationWaitTime: TimeInterval = 60



extension App {
    
    static func testInstance() -> App {
        
        let countries: Set<Country> = [Country.testInstance()]

        let app = App(id: "1", name: "My App", iconImageURL: URL(string: "http://www.anywhere.com")!, store: .ios, countries: countries, subscriptionID: "123", lastReviewsUpdate: Date())
        
        return app
    }
}


extension FullReview {
    
    static func testInstance() -> FullReview {
        
        let review = FullReview(author: "Mark", content: "This app is cool", id: "321", title: "Cool", upvotes: 0, version: "1.1", rating: 4, country: Country.testInstance(), appID: App.testInstance().id, date: Date())
        
        return review
    }
}


extension Country {
    
    static func testInstance() -> Country {
        
        let country = Country(id: "gb", name: "United Kingdom")
        
        return country
    }
}
