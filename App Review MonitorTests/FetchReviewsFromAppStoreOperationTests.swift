//
//  FetchReviewsFromAppStoreOperationTests.swift
//  App Review Monitor
//
//  Created by Mark Bridges on 11/12/2016.
//  Copyright © 2016 Mark Bridges. All rights reserved.
//

import XCTest
@testable import App_Review_Monitor


class FetchReviewsFromAppStoreOperationTests: XCTestCase {
    
    // MARK: Tests
    
    func testUpdateReviewsCallsTheCorrectURL() {
        
        let expectedURL = URL(string:"https://itunes.apple.com/gb/rss/customerreviews/page=1/id=1/sortby=mostrecent/xml")!
        
        let httpClientMock = HTTPClientMock()
        httpClientMock.mockedResults = [expectedURL : (HTTPClient.HTTPClientResult.success(data: Data()))]
        
        let myExpectation = expectation(description: "Call should complete")

        let operation = FetchReviewsFromAppStoreOperation(app: App.testInstance(), country: Country.testInstance(), httpClient: httpClientMock)
        operation.completionBlock = {
            myExpectation.fulfill()
        }
        operation.start()
        
        waitForExpectations(timeout: ExpectationWaitTime, handler: nil)
        
        XCTAssertEqual(httpClientMock.urlOfLastRequest, expectedURL)
    }
    
    
    func testFetchReviewsCreatesReviews() {
        
        let expectedURL = URL(string:"https://itunes.apple.com/gb/rss/customerreviews/page=1/id=1/sortby=mostrecent/xml")!
        
        let bundle = Bundle(for: FetchReviewsFromAppStoreOperationTests.self)
        let xmlFixtureUrl = bundle.url(forResource: "singlepagereviews", withExtension: "xml")!
        let data = try! Data(contentsOf: xmlFixtureUrl)
        
        let httpClientMock = HTTPClientMock()
        httpClientMock.mockedResults = [expectedURL : (HTTPClient.HTTPClientResult.success(data: data))]
        
        let operation = FetchReviewsFromAppStoreOperation(app: App.testInstance(), country: Country.testInstance(), httpClient: httpClientMock)
        
        let myExpectation = expectation(description: "Call should complete")
        
        operation.completionBlock = {
            switch operation.operationStatus {
                
            case .succeeded(let result):
                let reviews = result as! Set<FullReview>
                XCTAssertEqual(reviews.count, 9)
                let lastReview = reviews.sorted(by: { (reviewA, reviewB) -> Bool in
                    return Double(reviewA.id)! > Double(reviewB.id)!
                }).last!
                
                XCTAssertEqual(lastReview.author, "Lame ap")
                XCTAssertEqual(lastReview.content, "This is not an interesting ap.")
                XCTAssertEqual(lastReview.id, "665202219")
                XCTAssertEqual(lastReview.title, "Boring")
                XCTAssertEqual(lastReview.upvotes, 0)
                XCTAssertEqual(lastReview.version, "1.0")
                XCTAssertEqual(lastReview.rating, 1)
                XCTAssertEqual(lastReview.date, Date(timeIntervalSince1970: 1348718460))
                
                
            default:
                XCTFail("Call with valid response failed")
            }
            
            myExpectation.fulfill()
        }
        
        operation.start()

        
        waitForExpectations(timeout: ExpectationWaitTime, handler: nil)
    }
    
    func testFetchReviewsReturnsAnEmptyListForCountriesWithOutReviews() {
        
        let expectedURL = URL(string:"https://itunes.apple.com/gb/rss/customerreviews/page=1/id=1/sortby=mostrecent/xml")!
        
        let bundle = Bundle(for: FetchReviewsFromAppStoreOperationTests.self)
        let xmlFixtureUrl = bundle.url(forResource: "emptyreviewslist", withExtension: "xml")!
        let data = try! Data(contentsOf: xmlFixtureUrl)
        
        let httpClientMock = HTTPClientMock()
        httpClientMock.mockedResults = [expectedURL : (HTTPClient.HTTPClientResult.success(data: data))]
        
        let operation = FetchReviewsFromAppStoreOperation(app: App.testInstance(), country: Country.testInstance(), httpClient: httpClientMock)
        
        let myExpectation = expectation(description: "Call should complete")
        
        operation.completionBlock = {
            switch operation.operationStatus {
                
            case .succeeded(let result):
                let reviews = result as! Set<FullReview>
                XCTAssertEqual(reviews.count, 0)
                
            default:
                XCTFail("Call with valid response failed")
            }
            
            myExpectation.fulfill()
        }
        
        operation.start()
        
        
        waitForExpectations(timeout: ExpectationWaitTime, handler: nil)
        
    }
    
    func testFetchReviewsIteratesThroughXMLPages() {
        
        let expectedURL1 = URL(string:"https://itunes.apple.com/gb/rss/customerreviews/page=1/id=1/sortby=mostrecent/xml")!
        let expectedURL2 = URL(string:"https://itunes.apple.com/gb/rss/customerreviews/page=2/id=1/sortby=mostrecent/xml")!
        let expectedURL3 = URL(string:"https://itunes.apple.com/gb/rss/customerreviews/page=3/id=1/sortby=mostrecent/xml")!
        
        let bundle = Bundle(for: FetchReviewsFromAppStoreOperationTests.self)
        let xmlFixtureUrl1 = bundle.url(forResource: "multipagepage1", withExtension: "xml")!
        let data1 = try! Data(contentsOf: xmlFixtureUrl1)
        let xmlFixtureUrl2 = bundle.url(forResource: "multipagepage2", withExtension: "xml")!
        let data2 = try! Data(contentsOf: xmlFixtureUrl2)
        let xmlFixtureUrl3 = bundle.url(forResource: "emptyreviewslist", withExtension: "xml")!
        let data3 = try! Data(contentsOf: xmlFixtureUrl3)
        
        let httpClientMock = HTTPClientMock()
        httpClientMock.mockedResults = [
            expectedURL1 : (HTTPClient.HTTPClientResult.success(data: data1)),
            expectedURL2 : (HTTPClient.HTTPClientResult.success(data: data2)),
            expectedURL3 : (HTTPClient.HTTPClientResult.success(data: data3))
        ]
        
        let operation = FetchReviewsFromAppStoreOperation(app: App.testInstance(), country: Country.testInstance(), httpClient: httpClientMock)
        
        let myExpectation = expectation(description: "Call should complete")
        
        operation.completionBlock = {
            switch operation.operationStatus {
                
            case .succeeded(let result):
                let reviews = result as! Set<FullReview>
                XCTAssertEqual(reviews.count, 100)
                XCTAssert(httpClientMock.allUrlsCalled.contains(expectedURL1))
                XCTAssert(httpClientMock.allUrlsCalled.contains(expectedURL2))
                XCTAssert(httpClientMock.allUrlsCalled.contains(expectedURL3))
                
            default:
                XCTFail("Call with valid response failed")
            }
            
            myExpectation.fulfill()
        }
        
        operation.start()
        
        
        waitForExpectations(timeout: ExpectationWaitTime, handler: nil)
        
    }
    
}
