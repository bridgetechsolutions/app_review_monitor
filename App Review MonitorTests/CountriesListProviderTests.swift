//
//  CountriesListProviderTests.swift
//  App Review Monitor
//
//  Created by Mark Bridges on 12/12/2016.
//  Copyright © 2016 Mark Bridges. All rights reserved.
//

import XCTest
@testable import App_Review_Monitor


class CountriesListProviderTests: XCTestCase {
    
    func testInstantiatesAllCountries() {
        
        let provider = CountriesListBuilder()
        
        XCTAssertEqual(provider.allCountries.count, 155)
    }
    
    func testInstantiatesUK() {
        
        let home = Country(id: "GB", name: "United Kingdom")
        
        let provider = CountriesListBuilder()
        
        XCTAssert(provider.allCountries.contains(home))
    }
    
}
