//
//  NSMutableAttributedString+Highlight.swift
//  TSMApp
//
//  Created by Keith Moon on 20/10/2015.
//  Copyright © 2015 Travel Supermarket. All rights reserved.
//

import UIKit

extension NSMutableAttributedString {
    
    
    /**
     Apply attributes to a substring
     
     - parameter substrings: An array of substrings that the attributes should be applied to, if found. Each occurance will be applied. Case insensitive.
     - parameter attributes:     The colour to highlight the substrings.
     */
    
    func add(attributes: [String : AnyObject], substrings: [String]) {
        
        let castString = string as NSString
        
        for substring in substrings {
            
            var searchRange = NSMakeRange(0, castString.length)
            var foundRange = NSMakeRange(0, 0)
            
            repeat {
                
                foundRange = castString.range(of: substring, options: NSString.CompareOptions.caseInsensitive, range: searchRange)
                
                if foundRange.location != NSNotFound {
                    addAttributes(attributes, range: foundRange)
                    searchRange.location = foundRange.location+foundRange.length
                    searchRange.length = castString.length - searchRange.location
                }
                
            } while foundRange.location != NSNotFound
            
        }
    }
}
