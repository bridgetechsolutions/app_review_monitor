//
//  UITableView+LoadingFooter.swift
//  App Review Monitor
//
//  Created by Mark Bridges on 15/12/2016.
//  Copyright © 2016 Mark Bridges. All rights reserved.
//

import UIKit
import PureLayout

extension UITableView {
    
    var lastIndexPath: IndexPath? {
        
        let lastSectionIndex = numberOfSections - 1
        guard lastSectionIndex >= 0 else { return nil }
        
        let lastIndexInLastSection = numberOfRows(inSection: lastSectionIndex) - 1
        guard lastIndexInLastSection >= 0 else { return nil }
        
        return IndexPath(row: lastIndexInLastSection, section: lastSectionIndex)
    }
    
    var isLoading: Bool {
        get {
            return tableFooterView is LoadingView
        } set {
            if newValue {
                tableFooterView = LoadingView(frame: CGRect(x: 0, y: 0, width: frame.size.width, height: 50))
            } else {
                tableFooterView = nil
            }
        }
    }
    
    class LoadingView: UIView {
        
        let activityIndicatorView = UIActivityIndicatorView(activityIndicatorStyle: .gray)
        
        override init(frame: CGRect) {
            super.init(frame: frame)
            setup()
        }
        
        required init?(coder aDecoder: NSCoder) {
            super.init(coder: aDecoder)
            setup()
        }
        
        private func setup() {
            addSubview(activityIndicatorView)
            activityIndicatorView.autoCenterInSuperview()
            activityIndicatorView.startAnimating()
        }
    }
    
}
