//
//  NotificationBuilder.swift
//  App Review Monitor
//
//  Created by Mark Bridges on 18/12/2016.
//  Copyright © 2016 Mark Bridges. All rights reserved.
//

import CloudKit
import UserNotifications

enum NotificationCategory: String {
    case newReview = "com.bridgetech.notification.newreview"
    case subscriptionPrompt = "com.bridgetech.notification.subscription"
}

class NotificationBuilder {
    
    
    func cloudKitNotificationInfo(forApp app: App) -> CKNotificationInfo {
        
        let notification = CKNotificationInfo()
        notification.alertBody = L10n.notificationNewReviewTitle(app.name).string
        notification.shouldBadge = true
        notification.category = NotificationCategory.newReview.rawValue
        notification.shouldSendContentAvailable = true
        notification.desiredKeys = ["title", "content", "rating"]
        
        return notification
    }
    
    func notificationInfo(forApp app: App, fullReview: FullReview) -> UNMutableNotificationContent {
        
        let content = UNMutableNotificationContent()
        content.body = L10n.notificationNewReviewTitle(app.name).string
        content.sound = UNNotificationSound.default()
        content.categoryIdentifier = NotificationCategory.newReview.rawValue
        content.userInfo = ["review" : fullReview.dictionaryRepresentation]
        
        return content
    }
    

}
