//
//  UpdateAppOperation.swift
//  App Review Monitor
//
//  Created by Mark Bridges on 11/12/2016.
//  Copyright © 2016 Mark Bridges. All rights reserved.
//

import Foundation
import CloudKit


class FetchReviewsForSelectedCountriesOperation: AsynchronousOperation {
    
    enum FetchReviewsError: Error {
        case operationsFailed(errors: [Error])
    }
    
    let app: App
    let isPaginationEnabled: Bool
    /// How far back we should look for new reviews, nil means all the way
    let endDate: Date?
    let httpClient: HTTPClient

    private let innerOperationsQueue = OperationQueue()
    private var allOperations = Set<FetchReviewsFromAppStoreOperation>()
    
    
    required init(app: App, isPaginationEnabled: Bool, endDate: Date?, httpClient: HTTPClient = HTTPClient()) {
        self.app = app
        self.isPaginationEnabled = isPaginationEnabled
        self.endDate = endDate
        self.httpClient = httpClient
    }
    
    
    override func start() {
        super.start()
        
        let allDoneOperation = Operation()
        allDoneOperation.completionBlock = { [weak self] in
            
            guard let strongSelf = self else { return }
            
            let operationErrors: [Error] = strongSelf.allOperations.flatMap{ operation in
                switch operation.operationStatus {
                case .failed(let error):
                    return error
                default:
                    return nil
                }
            }
            
            guard operationErrors.isEmpty else {
                strongSelf.operationStatus = .failed(error: FetchReviewsError.operationsFailed(errors: operationErrors))
                return
            }
            
            let reviews: Set<FullReview> = strongSelf.allOperations.reduce(Set<FullReview>(), { (reviews, operation) -> Set<FullReview> in
                switch operation.operationStatus {
                case .succeeded(let result):
                    return reviews.union(result as! Set<FullReview>)
                default:
                    return reviews
                }
            })
            
            strongSelf.operationStatus = .succeeded(result: reviews)
        }
        
        app.countries.forEach { country in
            let operation = FetchReviewsFromAppStoreOperation(app: app, isPaginationEnabled: isPaginationEnabled, endDate: endDate, country: country, httpClient: httpClient)
            innerOperationsQueue.addOperation(operation)
            allOperations.insert(operation)
            allDoneOperation.addDependency(operation)
        }
        
        innerOperationsQueue.addOperation(allDoneOperation)
    }
    
}
