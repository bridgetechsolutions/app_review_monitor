//
//  SplitViewController.swift
//  App Review Monitor
//
//  Created by Mark Bridges on 14/12/2016.
//  Copyright © 2016 Mark Bridges. All rights reserved.
//

import UIKit
import CloudKit
import MBProgressHUD


class SplitViewController: UISplitViewController {
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        delegate = self
    }
    
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        UIApplication.inAppSubscriptionPromptHandler.doAppStartCheckPrompt(in: self)
    }
    
    
    func display(review: Review, toShare: Bool) {
        
        let progressHUD = MBProgressHUD.showAdded(to: view.window!, animated: true)
        progressHUD.label.numberOfLines = 0
        progressHUD.label.text = L10n.notificationProgressDownloadingFullReview.string
        progressHUD.removeFromSuperViewOnHide = true
        
        func complete(withError error: Error?) {
            if let error = error {
                let alertController = UIAlertController(error: error)
                present(alertController, animated: true, completion: nil)
            }
            
            progressHUD.hide(animated: true)
        }
        
        func display(fullReview: FullReview) {
            
            guard
                let appsNavigationController = viewControllers.first as? UINavigationController,
                let appsViewController = appsNavigationController.viewControllers.first as? AppsViewController else {
                    fatalError("Unexpected navigation heirachy")
            }
            
            appsViewController.selectApp(withID: fullReview.appID, completionHandler: { [weak self] result in
                switch result {
                case .success:
                    
                    guard
                        let reviewsNavigationController = self?.viewControllers.last as? UINavigationController else {
                            fatalError("Unexpected navigation heirachy")
                    }
                    
                    var potentialReviewsViewController = reviewsNavigationController.viewControllers.first as? ReviewsViewController
                    // Navigation heirachy comes out different on iPhone
                    if potentialReviewsViewController == nil {
                        potentialReviewsViewController = reviewsNavigationController.viewControllers.last?.childViewControllers.first as? ReviewsViewController
                    }
                    
                    guard let reviewsViewController = potentialReviewsViewController else {
                        fatalError("Unexpected navigation heirachy")
                    }
                    
                    reviewsViewController.select(fullReview: fullReview, toShare: toShare, completionHandler: { result in
                        switch result {
                        case .success:
                            complete(withError: nil)
                            if toShare {
                                reviewsViewController.share(fullReview, sourceView: reviewsViewController.view, length: .full)
                            }
                            
                        case .failure(let error):
                            complete(withError: error)
                        }
                    })
                    
                case .failure(let error):
                    complete(withError: error)
                    
                }
            })
        }
        
        // Cloud notifications don't have the complete review, so we need to download it first
        guard let fullReview = review as? FullReview else {
            let recordID = CKRecordID(recordName: review.id)
            
            FullReview.database.fetch(withRecordID: recordID, completionHandler: { record, error in
                
                if let error = error {
                    complete(withError: error)
                    return
                }
                guard
                    let record = record,
                    let fullReview = FullReview(record: record) else { fatalError("Can't deserialize record into review") }
                display(fullReview: fullReview)
            })
            return
        }
        
        display(fullReview: fullReview)
    }
    
}


extension SplitViewController: UISplitViewControllerDelegate {
    
    func splitViewController(_ splitViewController: UISplitViewController, collapseSecondary secondaryViewController: UIViewController, onto primaryViewController: UIViewController) -> Bool {
        return true
    }
}
