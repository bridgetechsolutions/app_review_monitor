//
//  ReviewsService.swift
//  App Review Monitor
//
//  Created by Mark Bridges on 10/12/2016.
//  Copyright © 2016 Mark Bridges. All rights reserved.
//

import Foundation
import SwiftyJSON


class ReviewsService {
    
    // MARK: Error Enums
    
    enum ReviewsServiceError: Error {
        case emptyResponse
        case undeserializableResponse
        case unexpectedResponse
    }
    
    // MARK: Result Enums
    
    enum JsonArrayResult {
        case success(data: Data)
        case failure(error: Error)
    }
    
    
    // MARK: Properties
    
    let httpClient: HTTPClient
    
    
    // MARK: Initialisation
    
    required init(httpClient: HTTPClient = HTTPClient()) {
        self.httpClient = httpClient
    }
    
    // MARK: Network calls
    
    func getReviews(forAppId appId: String, countryId: String, completion: @escaping ((JsonArrayResult) -> Void)) {
        
        guard
            let url = URL(string: "http://itunes.apple.com/\(countryId)/rss/customerreviews/id=\(appId)/json") else {
            fatalError("Unable to form URL for request")
        }
        
        _ = httpClient.makeNetworkRequest(with: url, completion: { result in
            
                switch result {
                case .success(let data):
                    completion(.success(data: data))
                    
                case .failure(let error):
                    completion(.failure(error: error))
                }
            
        })
    }
}
