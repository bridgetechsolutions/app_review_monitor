//
//  ActivityObjectsBuilder.swift
//  App Review Monitor
//
//  Created by Mark Bridges on 20/12/2016.
//  Copyright © 2016 Mark Bridges. All rights reserved.
//

import Foundation
import UIKit


let appReviewMonitorAppStoreID = "1188015880"


class ActivityObjectsBuilder {
    
    
    enum Length {
        case full
        case trimmed
    }
    
    
    func activityObjects(for review: Review, app: App, length: Length) -> [Any] {
        
        let starsString = String(stars:Int(review.rating))
        let theirAppNameString = app.name
        let myAppNameString = L10n.appName.string
        let theirAppLink = NSURL(appID: app.id)
        let myAppLink = NSURL(appID: appReviewMonitorAppStoreID)
        
        let title = L10n.shareMessageTitle(theirAppNameString).string
        
        switch length {
        case .full:
            
            let sentViaString = L10n.shareMessageSentVia(L10n.appName.string).string
            let fullString = NSMutableAttributedString(string: title + "\n" + starsString + "\n" + review.title + "\n" + review.content + "\n\n" + sentViaString + "\n")
            
            fullString.add(attributes: [NSForegroundColorAttributeName : UIColor.star], substrings: [starsString])
            fullString.add(attributes: [NSLinkAttributeName : theirAppLink], substrings: [theirAppNameString])
            fullString.add(attributes: [NSLinkAttributeName : myAppLink], substrings: [myAppNameString])
            fullString.add(attributes: [NSFontAttributeName : UIFont.preferredFont(forTextStyle: .headline)], substrings: [title])
            fullString.add(attributes: [NSFontAttributeName : UIFont.preferredFont(forTextStyle: .title3)], substrings: [review.title])
            fullString.add(attributes: [NSFontAttributeName : UIFont.preferredFont(forTextStyle: .callout)], substrings: [review.content])
            fullString.add(attributes: [NSFontAttributeName : UIFont.preferredFont(forTextStyle: .caption2)], substrings: [sentViaString])
            
            return [NSAttributedString(attributedString: fullString), theirAppLink]
            
        case .trimmed:
            
            let fullString = NSMutableAttributedString(string: (title + "\n" + starsString + "\n" + review.title + "\n" + review.content).truncated(toMaxLength: 140))

            fullString.add(attributes: [NSForegroundColorAttributeName : UIColor.star], substrings: [starsString])
            fullString.add(attributes: [NSLinkAttributeName : theirAppLink], substrings: [theirAppNameString])
            fullString.add(attributes: [NSFontAttributeName : UIFont.preferredFont(forTextStyle: .headline)], substrings: [title])
            fullString.add(attributes: [NSFontAttributeName : UIFont.preferredFont(forTextStyle: .title3)], substrings: [review.title])
            fullString.add(attributes: [NSFontAttributeName : UIFont.preferredFont(forTextStyle: .callout)], substrings: [review.content])
            
            return [NSAttributedString(attributedString: fullString), theirAppLink]
        }

    }
}


private extension String {
    
    init(stars: Int) {
        
        let maxStars = 5
        
        var starsString = ""
        
        while starsString.characters.count < maxStars {
            if starsString.characters.count < stars {
                starsString.append("★")
            } else {
                starsString.append("☆")
            }
        }
        
        self = starsString
    }
    
}


private extension NSURL {
    
    convenience init(appID: String) {
        self.init(string: "https://itunes.apple.com/app/id\(appID)")!
    }
}


private extension String {
    
    func truncated(toMaxLength length: Int, trailing: String? = "...") -> String {
        if self.characters.count > length {
            let trailingText = trailing ?? ""
            let uptoIndex = length - 1 - trailingText.characters.count
            let index = self.index(startIndex, offsetBy: uptoIndex)
            
            return self.substring(to: index) + trailingText
        } else {
            return self
        }
    }
}
