//
//  AppsSearchViewController .swift
//  App Review Monitor
//
//  Created by Mark Bridges on 13/12/2016.
//  Copyright © 2016 Mark Bridges. All rights reserved.
//

import UIKit
import AlamofireImage
import MBProgressHUD
import UserNotifications


protocol AppsSearchViewControllerDelegate: class {
    
    func appsSearchViewController(_ appsSearchViewController: AppsSearchViewController, completedWithResult result: AppsSearchViewController.Result)
}


class AppsSearchViewController: UITableViewController {
    
    enum NotificationAuthorisationResult {
        case granted
        case denied
        case failed(error: Error)
    }
    
    enum Result {
        case appSaved(app: App)
        case cancelled
    }
    
    weak var delegate: AppsSearchViewControllerDelegate?
    
    private let searchController = UISearchController(searchResultsController: nil)
    private let storeSearchService = StoreSearchService()
    
    private var results: [App] = []
    
    private lazy var currentCountry: Country = {
        let countriesBuilder = CountriesListBuilder()
        return countriesBuilder.currentCountry()
    } ()
    
    
    // MARK: Instance Lifecycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        navigationItem.title = L10n.searchTitle.string

        tableView.register(UINib(nibName: AppTableViewCell.identifier, bundle: nil), forCellReuseIdentifier: AppTableViewCell.identifier)
        
        // Setup the Search Controller
        searchController.searchResultsUpdater = self
        searchController.searchBar.delegate = self
        searchController.delegate = self
        definesPresentationContext = true
        searchController.dimsBackgroundDuringPresentation = false
        
        // Setup the Scope Bar
        searchController.searchBar.scopeButtonTitles = [
            L10n.searchStoreIphone.string,
            L10n.searchStoreIpad.string,
            L10n.searchStoreMac.string]
        tableView.tableHeaderView = searchController.searchBar
        
        searchController.isActive = true
    }
    
    
    // MARK: - Table View
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: AppTableViewCell.identifier, for: indexPath) as! AppTableViewCell
        
        let app = results[indexPath.row]
        
        cell.myTextLabel?.text = app.name
        
        let imageViewSize = cell.myImageView?.frame.size ?? .zero
        
        let filter: AspectScaledToFillSizeWithRoundedCornersFilter?
        switch app.store {
        case .iPhone, .iPad:
            filter = AspectScaledToFillSizeWithRoundedCornersFilter(
                size: imageViewSize,
                radius: imageViewSize.height * 0.12
            )
        case .mac:
            filter = nil
            
        }
        
        cell.myImageView?.af_setImage(
            withURL: app.iconImageURL,
            placeholderImage: nil,
            filter: filter
        )
        
        return cell
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return results.count
    }
    
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let app = results[indexPath.row]
        
        let progressHUD = MBProgressHUD.showAdded(to: view.window!, animated: true)
        progressHUD.label.numberOfLines = 0
        progressHUD.label.text = L10n.searchActivityProgressSubscribing.string
        progressHUD.removeFromSuperViewOnHide = true
    
        app.subscribeToUpdatesAndSave { [weak self] result in
            
            DispatchQueue.main.async { [weak self] in
                
                guard let strongSelf = self else { return }
                
                switch result {
                case .success(let appWithSubscription):
                    
                    self?.promptToAcceptPushNotifications { result in
                        
                        DispatchQueue.main.async { [weak self] in
                            
                            switch result {
                            case .granted:
                                progressHUD.mode = .customView
                                progressHUD.customView = UIImageView(image: Asset.tick.image)
                                progressHUD.label.text = L10n.searchActivitySuccessSubscribed.string
                                progressHUD.hide(animated: true, afterDelay: 4)
                                strongSelf.delegate?.appsSearchViewController(strongSelf, completedWithResult: .appSaved(app: appWithSubscription))
                                
                            case .denied:
                                progressHUD.hide(animated: true)
                                let alertController = UIAlertController(title: L10n.searchActivityFailurePushauthorisationTitle.string, message: L10n.searchActivityFailurePushauthorisationMessage.string)
                                self?.present(alertController, animated: true, completion: {
                                    strongSelf.delegate?.appsSearchViewController(strongSelf, completedWithResult: .appSaved(app: appWithSubscription))
                                })
                                
                            case .failed(let error):
                                progressHUD.hide(animated: true)
                                let alertController = UIAlertController(error: error, title: L10n.searchActivityFailurePushauthorisationTitle.string)
                                self?.present(alertController, animated: true, completion: {
                                    strongSelf.delegate?.appsSearchViewController(strongSelf, completedWithResult: .appSaved(app: appWithSubscription))
                                })
                            }
                        }
                        
                        _ = UIApplication.dataSyncer.updateReviews(forApp: app, completionHandler: { _ in
                            // TODO: Should we indicate this somehow
                        })
                    }
                    
                case .failure(let error):
                    progressHUD.hide(animated: true)
                    let alertController = UIAlertController(error: error)
                    self?.present(alertController, animated: true, completion: nil)
                }
                
            }
            
        }
        
    }
    
    func performSearch() {
        
        guard
            let searchTerm = searchController.searchBar.text,
            let store = App.Store(rawValue: searchController.searchBar.selectedScopeButtonIndex) else {
                return
        }
        
        guard searchTerm.isEmpty == false else { return }
        
        searchController.searchBar.isLoading = true
        
        storeSearchService.getApps(forSearchTerm: searchTerm, countryId: currentCountry.id, store: store) { [weak self] result in
            
            DispatchQueue.main.async { [weak self] in
                
                self?.searchController.searchBar.isLoading = false
                
                switch result {
                case .success(let apps):
                    self?.results = apps
                    
                case .failure(let error):
                    
                    switch error {
                    case StoreSearchService.StoreSearchServiceError.emptyResponse:
                        break
                        
                    default:
                        let alertController = UIAlertController(error: error)
                        self?.present(alertController, animated: true, completion: nil)
                        
                    }

                }
                
                self?.tableView.reloadData()
            }
            
        }
    }
    
    fileprivate func promptToAcceptPushNotifications(withCompletion completion: @escaping ((NotificationAuthorisationResult) -> (Void))) {
        UNUserNotificationCenter.current().requestAuthorization(options: [.alert, .sound, .badge]) { granted, error in
            if let error = error {
                log("Failed to get push notification authorisation: \(error.localizedDescription)")
                completion(.failed(error: error))
            } else if granted {
                UIApplication.shared.registerForRemoteNotifications()
                completion(.granted)
            } else {
                completion(.denied)
            }
        }
    }
}


// MARK: - UISearchBar Delegate

extension AppsSearchViewController: UISearchBarDelegate {
    
    func searchBar(_ searchBar: UISearchBar, selectedScopeButtonIndexDidChange selectedScope: Int) {
        performSearch()
    }
}


// MARK: - UISearchResultsUpdating Delegate

extension AppsSearchViewController: UISearchResultsUpdating {
    
    func updateSearchResults(for searchController: UISearchController) {
        performSearch()
    }
}


// MARK: - UISearchController Delegate

extension AppsSearchViewController: UISearchControllerDelegate {
    
    func didPresentSearchController(_ searchController: UISearchController) {
        searchController.searchBar.becomeFirstResponder()
    }
    
    func didDismissSearchController(_ searchController: UISearchController) {
        // Doing this in the willDismiss seems to prevent the view controller from popping
        delegate?.appsSearchViewController(self, completedWithResult: .cancelled)
    }


}

