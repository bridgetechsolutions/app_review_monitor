//
//  UIApplication+Singletons.swift
//  App Review Monitor
//
//  Created by Mark Bridges on 20/12/2016.
//  Copyright © 2016 Mark Bridges. All rights reserved.
//

import UIKit


extension UIApplication {
    
    class var dataSyncer: DataSyncer {
        guard let appDelegate = UIApplication.shared.delegate as? AppDelegate else {
            fatalError("Where's the app delegate")
        }
        
        return appDelegate.dataSyncer
    }
    
    class var inAppSubscriptionPromptHandler: InAppSubscriptionPromptHandler {
        guard let appDelegate = UIApplication.shared.delegate as? AppDelegate else {
            fatalError("Where's the app delegate")
        }
        
        return appDelegate.inAppSubscriptionPromptHandler
    }

    
}
