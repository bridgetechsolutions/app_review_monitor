//
//  UITableView+DeselectAll.swift
//  App Review Monitor
//
//  Created by Mark Bridges on 24/12/2016.
//  Copyright © 2016 Mark Bridges. All rights reserved.
//

import UIKit


extension UITableView {
    
    func deselectAllCells(_ animated: Bool = true) {
        if let selectedIndexPaths = indexPathsForVisibleRows {
            selectedIndexPaths.forEach {
                deselectRow(at: $0, animated: animated)
            }
        }
    }
}
