//
//  UpdateActivityStatusView.swift
//  App Review Monitor
//
//  Created by Mark Bridges on 23/12/2016.
//  Copyright © 2016 Mark Bridges. All rights reserved.
//

import UIKit
import PureLayout


class UpdateActivityStatusView: UIView {
    
    private var toolbarTopLabel = UILabel(forAutoLayout: ())
    private var toolbarBottomLabel = UILabel(forAutoLayout: ())
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        configure()
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        configure()
    }
    
    var isLoading: Bool = false {
        didSet {
            UIView.animate(withDuration: 0.2, animations: { [weak self] in
                guard let strongSelf = self else { return }
                strongSelf.toolbarTopLabel.isHidden = !strongSelf.isLoading
            })
        }
    }
    var lastUpdateDate: Date? = nil {
        didSet {
            updateBottomLabelText()
        }
    }
    
    private func configure() {
        let stackView = UIStackView(forAutoLayout: ())
        stackView.alignment = .fill
        stackView.distribution = .fillEqually
        stackView.axis = .vertical
        addSubview(stackView)
        stackView.autoPinEdgesToSuperviewEdges()
        stackView.addArrangedSubview(toolbarTopLabel)
        stackView.addArrangedSubview(toolbarBottomLabel)
        
        toolbarTopLabel.text = L10n.reviewsToolbarLoading.string
        
        toolbarTopLabel.textColor = UIColor.activityTopLabel
        toolbarBottomLabel.textColor = UIColor.activityBottomLabel
        toolbarTopLabel.font = UIFont.preferredFont(forTextStyle: .caption1)
        toolbarBottomLabel.font = UIFont.preferredFont(forTextStyle: .caption2)
        toolbarTopLabel.textAlignment = .center
        toolbarBottomLabel.textAlignment = .center
        
        let timer = Timer.new(every: 1.minute) { [weak self] in
            self?.updateBottomLabelText()
        }
        timer.start(modes: .defaultRunLoopMode)
    }
    
    
    private func updateBottomLabelText() {
        
        guard
            let lastUpdateDate = lastUpdateDate else {
                UIView.animate(withDuration: 0.2) {
                    self.toolbarBottomLabel.isHidden = true
                }
                return
        }
        
        UIView.animate(withDuration: 0.2) {
            self.toolbarBottomLabel.isHidden = false
        }
        
        let timeInterval = Date().timeIntervalSince(lastUpdateDate)
        
        guard timeInterval >= 60 else {
            toolbarBottomLabel.text = L10n.reviewsToolbarLastupdateJustnow.string
            return
        }
        
        let formatter = DateComponentsFormatter()
        formatter.unitsStyle = .full
        formatter.includesApproximationPhrase = false
        formatter.includesTimeRemainingPhrase = false
        formatter.maximumUnitCount = 1
        
        guard
            let outputString = formatter.string(from: timeInterval) else {
                fatalError("Cannot get time interval since last update")
        }
        
        toolbarBottomLabel.text = L10n.reviewsToolbarLastupdate(outputString).string
    }
}
