// Generated using SwiftGen, by O.Halligon — https://github.com/AliSoftware/SwiftGen

import Foundation

// swiftlint:disable file_length
// swiftlint:disable line_length

// swiftlint:disable type_body_length
enum L10n {
  /// App Review Monitor
  case appName
  /// Start monitoring your first app
  case appsEmptyStateMessage
  /// Welcome To App Review Monitor
  case appsEmptyStateTitle
  /// Apps
  case appsTitle
  /// Something Went Wrong
  case errorAlertGenericTitle
  /// Cancel
  case globalCancel
  /// OK
  case globalOk
  /// Share...
  case notificationNewReviewShareButtonTitle
  /// New review for %@
  case notificationNewReviewTitle(String)
  /// Selecting review
  case notificationProgressDownloadingFullReview
  /// by %@ from %@
  case reviewAuthorAndCountry(String, String)
  /// No reviews have been found yet
  case reviewsEmptyStateMessage
  /// Reviews
  case reviewsEmptyStateTitle
  /// Reviews
  case reviewsTitle
  /// Last updated: %@ ago
  case reviewsToolbarLastupdate(String)
  /// Last updated: Just now
  case reviewsToolbarLastupdateJustnow
  /// Looking for new reviews...
  case reviewsToolbarLoading
  /// Please enable push notifications in settings to recieve notifications for new reviews of this app
  case searchActivityFailurePushauthorisationMessage
  /// Push Notifications Not Authorised
  case searchActivityFailurePushauthorisationTitle
  /// Saving subscription
  case searchActivityProgressSubscribing
  /// Subscribed!\nYou'll now be notified of new reviews for this app.
  case searchActivitySuccessSubscribed
  /// iPad
  case searchStoreIpad
  /// iPhone
  case searchStoreIphone
  /// Mac
  case searchStoreMac
  /// Search
  case searchTitle
  /// Full Review
  case shareAlertOptionFull
  /// Shortened Review
  case shareAlertOptionShort
  /// Share Review
  case shareAlertTitle
  /// Sent via %@
  case shareMessageSentVia(String)
  /// Review for %@
  case shareMessageTitle(String)
  /// Restored!
  case subscriptionActivityRestored
  /// Restoring Subscription
  case subscriptionActivityRestoring
  /// Subscribed!
  case subscriptionActivitySubscribed
  /// Subscribing
  case subscriptionActivitySubscribing
  /// Remind Me Later
  case subscriptionAlertButtonTitleLater
  /// Restore Previous Subscription
  case subscriptionAlertButtonTitleRestore
  /// Subscribe (%@ for %@)
  case subscriptionAlertButtonTitleSubscribe(String, String)
  /// Your subscription has now expired; you won't be getting anymore notifications when new reviews come in. Please renew your subscription, so you don't miss out.
  case subscriptionAlertSubscriptionExpiredMessage
  /// Subscription Expired
  case subscriptionAlertSubscriptionExpiredTitle
  /// Your trial has now expired; you won't be getting anymore notifications when new reviews come in. Please subscribe, so you don't miss out.
  case subscriptionAlertTrialExpiredMessage
  /// Trial Expired
  case subscriptionAlertTrialExpiredTitle
  /// Please renew your subscription to keep recieving notifications for new reviews.
  case subscriptionNotificationSubscriptionExpiredMessage
  /// Subscription Expired
  case subscriptionNotificationSubscriptionExpiredTitle
  /// Please subscribe to keep recieving notifications for new reviews.
  case subscriptionNotificationTrialExpiredMessage
  /// Trial Expired
  case subscriptionNotificationTrialExpiredTitle
  /// If you find recieving notifications for new reviews helpful, after %@ please subscribe.
  case subscriptionNotificationTrialStartedMessage(String)
  /// Thanks for choosing App Review Monitor
  case subscriptionNotificationTrialStartedTitle
}
// swiftlint:enable type_body_length

extension L10n: CustomStringConvertible {
  var description: String { return self.string }

  var string: String {
    switch self {
      case .appName:
        return L10n.tr(key: "app.name")
      case .appsEmptyStateMessage:
        return L10n.tr(key: "apps.empty-state.message")
      case .appsEmptyStateTitle:
        return L10n.tr(key: "apps.empty-state.title")
      case .appsTitle:
        return L10n.tr(key: "apps.title")
      case .errorAlertGenericTitle:
        return L10n.tr(key: "error.alert.generic-title")
      case .globalCancel:
        return L10n.tr(key: "global.cancel")
      case .globalOk:
        return L10n.tr(key: "global.ok")
      case .notificationNewReviewShareButtonTitle:
        return L10n.tr(key: "notification.new-review.share-button.title")
      case .notificationNewReviewTitle(let p0):
        return L10n.tr(key: "notification.new-review.title", p0)
      case .notificationProgressDownloadingFullReview:
        return L10n.tr(key: "notification.progress.downloading-full-review")
      case .reviewAuthorAndCountry(let p0, let p1):
        return L10n.tr(key: "review.author-and-country", p0, p1)
      case .reviewsEmptyStateMessage:
        return L10n.tr(key: "reviews.empty-state.message")
      case .reviewsEmptyStateTitle:
        return L10n.tr(key: "reviews.empty-state.title")
      case .reviewsTitle:
        return L10n.tr(key: "reviews.title")
      case .reviewsToolbarLastupdate(let p0):
        return L10n.tr(key: "reviews.toolbar.lastupdate", p0)
      case .reviewsToolbarLastupdateJustnow:
        return L10n.tr(key: "reviews.toolbar.lastupdate.justnow")
      case .reviewsToolbarLoading:
        return L10n.tr(key: "reviews.toolbar.loading")
      case .searchActivityFailurePushauthorisationMessage:
        return L10n.tr(key: "search.activity.failure.pushauthorisation.message")
      case .searchActivityFailurePushauthorisationTitle:
        return L10n.tr(key: "search.activity.failure.pushauthorisation.title")
      case .searchActivityProgressSubscribing:
        return L10n.tr(key: "search.activity.progress.subscribing")
      case .searchActivitySuccessSubscribed:
        return L10n.tr(key: "search.activity.success.subscribed")
      case .searchStoreIpad:
        return L10n.tr(key: "search.store.ipad")
      case .searchStoreIphone:
        return L10n.tr(key: "search.store.iphone")
      case .searchStoreMac:
        return L10n.tr(key: "search.store.mac")
      case .searchTitle:
        return L10n.tr(key: "search.title")
      case .shareAlertOptionFull:
        return L10n.tr(key: "share.alert.option.full")
      case .shareAlertOptionShort:
        return L10n.tr(key: "share.alert.option.short")
      case .shareAlertTitle:
        return L10n.tr(key: "share.alert.title")
      case .shareMessageSentVia(let p0):
        return L10n.tr(key: "share.message.sent-via", p0)
      case .shareMessageTitle(let p0):
        return L10n.tr(key: "share.message.title", p0)
      case .subscriptionActivityRestored:
        return L10n.tr(key: "subscription.activity.restored")
      case .subscriptionActivityRestoring:
        return L10n.tr(key: "subscription.activity.restoring")
      case .subscriptionActivitySubscribed:
        return L10n.tr(key: "subscription.activity.subscribed")
      case .subscriptionActivitySubscribing:
        return L10n.tr(key: "subscription.activity.subscribing")
      case .subscriptionAlertButtonTitleLater:
        return L10n.tr(key: "subscription.alert.button-title.later")
      case .subscriptionAlertButtonTitleRestore:
        return L10n.tr(key: "subscription.alert.button-title.restore")
      case .subscriptionAlertButtonTitleSubscribe(let p0, let p1):
        return L10n.tr(key: "subscription.alert.button-title.subscribe", p0, p1)
      case .subscriptionAlertSubscriptionExpiredMessage:
        return L10n.tr(key: "subscription.alert.subscription-expired.message")
      case .subscriptionAlertSubscriptionExpiredTitle:
        return L10n.tr(key: "subscription.alert.subscription-expired.title")
      case .subscriptionAlertTrialExpiredMessage:
        return L10n.tr(key: "subscription.alert.trial-expired.message")
      case .subscriptionAlertTrialExpiredTitle:
        return L10n.tr(key: "subscription.alert.trial-expired.title")
      case .subscriptionNotificationSubscriptionExpiredMessage:
        return L10n.tr(key: "subscription.notification.subscription-expired.message")
      case .subscriptionNotificationSubscriptionExpiredTitle:
        return L10n.tr(key: "subscription.notification.subscription-expired.title")
      case .subscriptionNotificationTrialExpiredMessage:
        return L10n.tr(key: "subscription.notification.trial-expired.message")
      case .subscriptionNotificationTrialExpiredTitle:
        return L10n.tr(key: "subscription.notification.trial-expired.title")
      case .subscriptionNotificationTrialStartedMessage(let p0):
        return L10n.tr(key: "subscription.notification.trial-started.message", p0)
      case .subscriptionNotificationTrialStartedTitle:
        return L10n.tr(key: "subscription.notification.trial-started.title")
    }
  }

  private static func tr(key: String, _ args: CVarArg...) -> String {
    let format = NSLocalizedString(key, comment: "")
    return String(format: format, locale: Locale.current, arguments: args)
  }
}

func tr(_ key: L10n) -> String {
  return key.string
}
