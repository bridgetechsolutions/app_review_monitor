//
//  FetchAndUploadReviewsForAppOperation.swift
//  App Review Monitor
//
//  Created by Mark Bridges on 22/12/2016.
//  Copyright © 2016 Mark Bridges. All rights reserved.
//

import Foundation
import CloudKit


class FetchAndUploadReviewsForAppOperation: AsynchronousOperation {
    
    enum FetchAndUploadReviewsResult {
        case success(newReviews: Set<FullReview>)
        case failure(error: Error)
    }
    
    enum FetchAndUploadReviewsForAppError: Error {
        case operationsFailed(errors: [Error])
    }
    
    typealias FetchAndUploadReviewsCompletion = ((FetchAndUploadReviewsResult) -> (Void))
    
    let app: App
    let isPaginationEnabled: Bool
    /// How far back we should look for new reviews, nil means all the way
    let endDate: Date?
    let httpClient: HTTPClient
    
    private let innerOperationsQueue = OperationQueue()
    private var completionHandlers = [FetchAndUploadReviewsCompletion]()
    
    private(set)var newReviews = Set<FullReview>()
    
    required init(app: App, isPaginationEnabled: Bool, endDate: Date?, httpClient: HTTPClient = HTTPClient(), completionHandler: @escaping FetchAndUploadReviewsCompletion) {
        self.app = app
        self.isPaginationEnabled = isPaginationEnabled
        self.endDate = endDate
        self.httpClient = httpClient
        super.init()
        add(completionHandler: completionHandler)
        completionBlock = { [weak self] in
            
            guard let strongSelf = self else { return }
            
            let fetchAndUploadResult: FetchAndUploadReviewsResult
            
            switch strongSelf.operationStatus {
            case .succeeded(_):
                fetchAndUploadResult = .success(newReviews: strongSelf.newReviews)

            case .failed(let error):
                fetchAndUploadResult = .failure(error: error)
                
            default:
                fatalError("Completion block called with incorrect status \(strongSelf.operationStatus)")
            }
            
            strongSelf.completionHandlers.forEach({ completionHandler in
                completionHandler(fetchAndUploadResult)
            })
            
        }
    }
    
    func add(completionHandler: @escaping FetchAndUploadReviewsCompletion) {
        completionHandlers.append(completionHandler)
    }
    
    override func start() {
        super.start()

        let fetchReviewsOperation = FetchReviewsForSelectedCountriesOperation(app: app, isPaginationEnabled: isPaginationEnabled, endDate: endDate)
        fetchReviewsOperation.completionBlock = { [weak self, fetchReviewsOperation] in
            
            guard let strongSelf = self else { return }
            
            switch fetchReviewsOperation.operationStatus {
            case .succeeded(let result):
                
                let reviews = result as? Set<FullReview> ?? Set<FullReview>()
                
                log("Fetched \(reviews.count) reviews for: \(strongSelf.app)")
                
                EventLogger.logEvent(named: "retrieved_all_reviews", parameters: [
                    "number_of_reviews" : reviews.count
                    ])
                
                guard reviews.isEmpty == false else {
                    strongSelf.finishOperationUpdatingTheAppsLastReviewsUpdateProperty()
                    return
                }
                
                strongSelf.upload(reviews)
                
            case .failed(let error):
                strongSelf.operationStatus = .failed(error: error)
                
            default:
                fatalError("Completion called in unexpected state")
            }
        }
        
        innerOperationsQueue.addOperation(fetchReviewsOperation)
    }
    
    
    private func upload(_ reviews: Set<FullReview>) {
        
        let uploadOperation = UploadReviewsOperation(reviews: reviews)
        uploadOperation.completionBlock = { [unowned uploadOperation, weak self] in
            
            switch uploadOperation.operationStatus {
            case .succeeded(let result):
                self?.newReviews = result as! Set<FullReview>
                self?.finishOperationUpdatingTheAppsLastReviewsUpdateProperty()
                
            case .failed(let error):
                self?.operationStatus = .failed(error: error)
                
            default:
                fatalError("Completion called in unexpected state")
            }
            
        }
        
        innerOperationsQueue.addOperation(uploadOperation)
    }
    
    
    private func finishOperationUpdatingTheAppsLastReviewsUpdateProperty() {
        
        let updatedApp = App(id: app.id,
                             name: app.name,
                             iconImageURL: app.iconImageURL,
                             store: app.store,
                             countries: app.countries,
                             subscriptionID: app.subscriptionID,
                             lastReviewsUpdate: Date())
            
        updatedApp.save { [weak self] result in
            
            guard let strongSelf = self else { return }
            
            switch result {
            case .success:
                strongSelf.operationStatus = .succeeded(result: strongSelf.newReviews)
                
            case .failure(let error):
                strongSelf.operationStatus = .failed(error: error)
                
            }
        }
        
    }
    
}
