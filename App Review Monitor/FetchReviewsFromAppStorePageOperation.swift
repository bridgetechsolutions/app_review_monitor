//
//  FetchReviewsFromAppStorePageOperation.swift
//  App Review Monitor
//
//  Created by Mark Bridges on 15/12/2016.
//  Copyright © 2016 Mark Bridges. All rights reserved.
//

import Foundation
import SWXMLHash


private let reviewsResponseDateFormatter: DateFormatter = {
    let dateFormatter = DateFormatter()
    dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ssZZZZZ"
    dateFormatter.locale = Locale(identifier: "en_US_POSIX")
    return dateFormatter
}()


class FetchReviewsFromAppStorePageOperation: AsynchronousOperation {
    
    enum FetchReviewsFromAppStoreOperationError: Error {
        case unexpectedResponse
        case unableToParseNumbers
        case unableToFindLastPageNumber
    }
    
    let app: App
    let country: Country
    let pageNumber: Int
    let httpClient: HTTPClient
    
    private let operationQueue = OperationQueue()
    
    
    required init(app: App, country: Country, pageNumber: Int, httpClient: HTTPClient = HTTPClient()) {
        self.app = app
        self.country = country
        self.pageNumber = pageNumber
        self.httpClient = httpClient
    }
    
    
    override func start() {
        super.start()
        
        guard
            let url = URL(string: "https://itunes.apple.com/\(country.id)/rss/customerreviews/page=\(pageNumber)/id=\(app.id)/sortby=mostrecent/xml") else {
                fatalError("Unable to form URL for request")
        }
        
        _ = httpClient.makeNetworkRequest(with: url, completion: { [weak self] result in
            
            guard let strongSelf = self else { return }
            
            switch result {
            case .success(let data):
                
                let xml = SWXMLHash.config {
                    config in
                    config.shouldProcessLazily = false
                    }.parse(data)
                
                let feed = xml["feed"]
                
                var reviews = Set<FullReview>()
                
                feed["entry"].all.dropFirst().forEach{ indexer in
                    do {
                        reviews.insert(try strongSelf.newReview(withXMLNode: indexer))
                    } catch {
                        self?.operationStatus = .failed(error: error)
                    }
                }
                
                let areThereMorePages: Bool
                do {
                    guard let lastPageLinkString: String = try feed["link"].withAttr("rel", "last").element?.attribute(by: "href")?.text else {
                        throw FetchReviewsFromAppStoreOperationError.unableToFindLastPageNumber
                    }
                    let lastPageNumber = try strongSelf.pageNumber(fromURLString: lastPageLinkString)
                    areThereMorePages = lastPageNumber > strongSelf.pageNumber
                } catch {
                    areThereMorePages = false
                }
                
                let result = (reviews: reviews, areThereMorePages: areThereMorePages)
                log("Fetched reviews \(reviews) for app: \(strongSelf.app) country:\(strongSelf.country), page: \(strongSelf.pageNumber) morePages:\(areThereMorePages)")
                self?.operationStatus = .succeeded(result: result)
                
            case .failure(let error):
                log("Failed to fetch reviews for app: \(strongSelf.app) country:\(strongSelf.country), page: \(strongSelf.pageNumber) error: \(error)")
                self?.operationStatus = .failed(error: error)
            }
            
        })
    }
    
    
    private func newReview(withXMLNode node: XMLIndexer) throws -> FullReview {
        
        let updated: String = try node["updated"].value()
            let author: String = try node["author"]["name"].value()
            let id: String = try node["id"].value()
            let version: String = try node["im:version"].value()
            let title: String = try node["title"].value()
            let content: String? = try node["content"].withAttr("type", "text").element?.text
            let ratingString: String = try node["im:rating"].value()
            let upvotesString: String = try node["im:voteCount"].value()
        
            guard
                let nonOptionalContent = content,
                let date = reviewsResponseDateFormatter.date(from: updated),
                let rating = Float(ratingString),
                let upvotes = Int(upvotesString) else {
                    throw FetchReviewsFromAppStoreOperationError.unableToParseNumbers
        }
        
        let review = FullReview(
            author: author,
            content: nonOptionalContent,
            id: id,
            title: title,
            upvotes: upvotes,
            version: version,
            rating: rating,
            country: country,
            appID: app.id,
            date: date)
        
        return review
    }
    
    
    private func pageNumber(fromURLString urlString: String) throws -> Int {
        
        guard
            let lastPageURL = URL(string: urlString),
            let pageComponent = lastPageURL.pathComponents.filter({ return $0.hasPrefix("page=") }).first,
            let lastPageNumber = Int(pageComponent.replacingOccurrences(of: "page=", with: "")) else {
            throw FetchReviewsFromAppStoreOperationError.unableToFindLastPageNumber
        }
        
        return lastPageNumber
    }

    
}
