//
//  Logger.swift
//  PreviewVideoConverter
//
//  Created by Mark Bridges on 17/07/2016.
//  Copyright © 2016 BridgeTech. All rights reserved.
//

import Foundation
import Crashlytics
import CocoaLumberjack


func log(_ log: String) {
    
    DDLogDebug(log)

    CLSLogv(log, getVaList([]))
    #if DEBUG
//    print(log)
    #endif
}
