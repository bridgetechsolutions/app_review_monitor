//
//  UploadReviewsOperation.swift
//  App Review Monitor
//
//  Created by Mark Bridges on 15/12/2016.
//  Copyright © 2016 Mark Bridges. All rights reserved.
//

import Foundation
import CloudKit

class UploadReviewsOperation: AsynchronousOperation {
    
    enum FetchRecordsResult {
        case success(recordNames: Set<String>)
        case failure(error: Error)
    }
    
    enum UploadReviewsError: Error {
        case operationsFailed(errors: [Error])
    }
    
    // This seems to be the point that it fails
    private let MaxCKOperationLimit = 400
    
    let reviews: Set<FullReview>
    
    
    lazy var innerOperationsQueue: OperationQueue = {
        let operationQueue = OperationQueue()
        operationQueue.maxConcurrentOperationCount = 1
        return operationQueue
    }()
    private var allOperations = Set<CKOperation>()
    private var allSavedRecords = Set<CKRecord>()
    private var allErrors: [Error] = []
    
    
    required init(reviews: Set<FullReview>) {
        self.reviews = reviews
    }
    
    
    override func start() {
        super.start()
        
        let allReviewRecords = reviews.map { $0.record }
        
        fetchExisting(records: allReviewRecords) { [weak self] result in
            switch result {
            case .success(let recordNames):
                
                let newReviews = allReviewRecords.filter{ record in
                    return recordNames.contains(record.recordID.recordName) == false
                }
                
                log("New reviews \(newReviews.count)")
                
                if newReviews.isEmpty  {
                    self?.operationStatus = .succeeded(result: Set<FullReview>())
                } else {
                    self?.upload(records: newReviews)
                }
                
            case .failure(let error):
                self?.operationStatus = .failed(error: error)
            }
            
        }
        
    }
    
    private func fetchExisting(records: [CKRecord], completionHandler: @escaping ((FetchRecordsResult) -> (Void))) {
        
        let recordIDs = records.map { $0.recordID }
        
        let reviewRecordChunks: [[CKRecord]] = stride(from: 0, to: records.count, by: MaxCKOperationLimit).map {
            let end = records.endIndex
            let chunkEnd = records.index($0, offsetBy: MaxCKOperationLimit, limitedBy: end) ?? end
            return Array(records[$0..<chunkEnd])
        }
        
        var allFetchErrors = [Error]()
        var allFetchedRecordNames = Set<String>()
        
        let allRecordsFetchedOperation = Operation()
        allRecordsFetchedOperation.completionBlock = { [weak self] in
            
            guard let strongSelf = self else { return }
            
            guard allFetchErrors.isEmpty else {
                completionHandler(.failure(error: UploadReviewsError.operationsFailed(errors: strongSelf.allErrors)))
                log("Failed to fetched existing reviews from cloud \(strongSelf.reviews) \(strongSelf.allErrors)")
                return
            }
            
            log("Fetched all \(allFetchedRecordNames.count) existing reviews")
            
            completionHandler(.success(recordNames: allFetchedRecordNames))
        }
        
        func startFetchOperation(forRecords records: [CKRecord]) {
            
            log("Starting fetch for \(records.count) existing records")
            
            let recordIDs = records.map{ $0.recordID }
            
            let fetchReviewsOperation = CKFetchRecordsOperation(recordIDs: recordIDs)
            fetchReviewsOperation.desiredKeys = []
            fetchReviewsOperation.database = FullReview.database
            fetchReviewsOperation.fetchRecordsCompletionBlock = { recordsByRecordID, error in
                if let error = error {
                    if error.isForUnknownItemsOnly == false {
                        completionHandler(.failure(error: error))
                        return
                    }
                }
                
                guard let recordsByRecordID = recordsByRecordID else {
                    log("No existing records found")
                    completionHandler(.success(recordNames: Set<String>()))
                    return
                }
                
                let recordIDs = recordsByRecordID.keys
                let recordNames = Set(recordIDs.map { $0.recordName })
                
                log("Fetched \(recordNames.count) existing records")
                
                allFetchedRecordNames.formUnion(recordNames)
            }
            
            allRecordsFetchedOperation.addDependency(fetchReviewsOperation)
            allOperations.insert(fetchReviewsOperation)
            innerOperationsQueue.addOperation(fetchReviewsOperation)
        }
        
        reviewRecordChunks.forEach { recordsChunk in
            startFetchOperation(forRecords: recordsChunk)
        }
        
        innerOperationsQueue.addOperation(allRecordsFetchedOperation)
    }
    
    private func upload(records: [CKRecord]) {
        
        let reviewRecordChunks: [[CKRecord]] = stride(from: 0, to: records.count, by: MaxCKOperationLimit).map {
            let end = records.endIndex
            let chunkEnd = records.index($0, offsetBy: MaxCKOperationLimit, limitedBy: end) ?? end
            return Array(records[$0..<chunkEnd])
        }
        
        let allRecordsUploadedOperation = Operation()
        allRecordsUploadedOperation.completionBlock = { [weak self] in
            
            guard let strongSelf = self else { return }
            
            guard strongSelf.allErrors.isEmpty else {
                strongSelf.operationStatus = .failed(error: UploadReviewsError.operationsFailed(errors: strongSelf.allErrors))
                log("Failed to upload reviews to cloud: \(strongSelf.reviews) \(strongSelf.allErrors)")
                return
            }
            
            let savedReviews = Set(strongSelf.allSavedRecords.flatMap{ return FullReview(record:$0) })
            log("Finished saving \(savedReviews.count) new reviews")
            
            strongSelf.operationStatus = .succeeded(result: savedReviews)
        }
        
        func startUploadOperation(forRecords records: [CKRecord]) {
            
            let uploadReviewsOperation = CKModifyRecordsOperation(recordsToSave: records, recordIDsToDelete: nil)
            uploadReviewsOperation.database = FullReview.database
            uploadReviewsOperation.isAtomic = false
            uploadReviewsOperation.savePolicy = .allKeys
            uploadReviewsOperation.modifyRecordsCompletionBlock = { [weak self] savedRecords, deletedRecords, error in
                
                guard
                    let strongSelf = self else {
                        return
                }
                
                if let error = error {
                    strongSelf.allErrors.append(error)
                }
                
                log("Uploaded \(savedRecords?.count ?? 0) new reviews")
                
                if let savedRecords = savedRecords {
                    strongSelf.allSavedRecords = strongSelf.allSavedRecords.union(Set(savedRecords))
                }
            }
            
            allRecordsUploadedOperation.addDependency(uploadReviewsOperation)
            allOperations.insert(uploadReviewsOperation)
            innerOperationsQueue.addOperation(uploadReviewsOperation)
        }
        
        reviewRecordChunks.forEach { recordsChunk in
            startUploadOperation(forRecords: recordsChunk)
        }
        
        innerOperationsQueue.addOperation(allRecordsUploadedOperation)
    }
    
}


extension Error {
    
    /// Does this error purely represent that these items weren't found on the server
    var isForUnknownItemsOnly: Bool {
        
        guard let cloudError = self as? CKError else { return false }
        
        switch cloudError.errorCode {
        case CKError.Code.partialFailure.rawValue:
            guard
                let partialsErrorsDictionary = cloudError.partialErrorsByItemID,
                let partialsErrors = Array(partialsErrorsDictionary.values) as? [CKError] else {
                return false
            }

            let unknownItemErrors = partialsErrors.filter { $0.code.rawValue == CKError.Code.unknownItem.rawValue }
            return partialsErrors.count == unknownItemErrors.count
            
        case CKError.Code.unknownItem.rawValue:
            return true
            
        default:
            return false
            
        }
        
    }
    
}
