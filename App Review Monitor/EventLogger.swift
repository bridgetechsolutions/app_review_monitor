//
//  EventLogger.swift
//  Geek Quiz
//
//  Created by Mark Bridges on 12/05/2016.
//  Copyright © 2016 Hackney Homes. All rights reserved.
//

import Foundation
import Crashlytics
import Fabric
import Firebase


class EventLogger {
    
    
    static func logEvent(named eventName: String, parameters: [String : Any] = [:]) {
        
        guard let objectParameters = parameters as? [String : NSObject] else {
            fatalError("Unable to convert parameters into required type: \(parameters)")
        }
        
        FIRAnalytics.logEvent(withName: eventName, parameters: objectParameters)
        Answers.logCustomEvent(withName: eventName, customAttributes: objectParameters)
    }
    
    
    static func logShareEvent(withMethodName methodName: String, contentType: String, contentId: String) {
        
        Answers.logShare(withMethod: methodName, contentName: "review", contentType: contentType, contentId: contentId, customAttributes: nil)
        
        let dictionary: [String : NSObject] = [
                "length" : contentType as NSObject,
                "type" : methodName as NSObject
                ]
        
        FIRAnalytics.logEvent(withName: "shared_review", parameters: dictionary)
    }
    
    static func logPurchase(withPrice price: NSDecimalNumber, currency: String, success: Bool, itemId: String) {
        
        Answers.logPurchase(withPrice: price,
                            currency: currency,
                            success: NSNumber(value: success),
                            itemName: nil,
                            itemType: nil,
                            itemId: itemId,
                            customAttributes: nil)
        
        let dictionary = [
            "price" : price as NSObject,
            "currency" : currency as NSObject,
            "success" : NSNumber(value: success),
            "itemId" : itemId as NSObject
        ]
        
        FIRAnalytics.logEvent(withName: "made_purchase", parameters: dictionary)
    }
}



