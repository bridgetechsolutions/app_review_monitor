//
//  HTTPClient.swift
//  NetAborter
//
//  Created by Mark Bridges on 24/11/2016.
//  Copyright © 2016 BridgeTech. All rights reserved.
//

import Foundation

class HTTPClient {

    // MARK: Result enum

    enum HTTPClientResult {
        case success(data: Data)
        case failure(error: Error)
    }
    
    enum HTTPClientError: Error {
        case noData
    }

    // MARK: Properties

    let session: NetworkSession

    // MARK: Initialisation

    required init(session: NetworkSession = URLSession(configuration: URLSessionConfiguration.default)) {
        self.session = session
    }

    // MARK: Network call

    func makeNetworkRequest(with url: URL, completion: @escaping ((HTTPClientResult) -> Void)) -> URLSessionDataTask  {
        
        return session.startDataTask(with: url, completionHandler: { (data, _, error) -> Void in
            
            if let error = error {
                completion(.failure(error: error))
                return
            }
            
            guard let data = data else {
                completion(.failure(error: HTTPClientError.noData))
                return
            }
            
            completion(.success(data: data))
        })
    }
    
}

// MARK: NetworkSession protocol - (To aid in testing we don't use a URLSession directly, but instead a NetworkSession protocol)

protocol NetworkSession {
    func startDataTask(with url: URL, completionHandler: @escaping (Data?, URLResponse?, Error?) -> Swift.Void) -> URLSessionDataTask
}

// MARK: URLSession NetworkSession Conformance

extension URLSession: NetworkSession {

    func startDataTask(with url: URL, completionHandler: @escaping (Data?, URLResponse?, Error?) -> Swift.Void) -> URLSessionDataTask {
        let dataTask = self.dataTask(with: url, completionHandler: completionHandler)
        dataTask.resume()
        
        return dataTask
    }
}


//class NetworkOperation: AsynchronousOperation {
//    
//    typealias DataTaskResponseValues = ((Data?, URLResponse?, Error?) -> (Void))
//    
//    let url: URL
//    let session: URLSession
//    let completionHandler: DataTaskResponseValues
//    
//    
//    init(url: URL, session: NetworkSession, completionHandler: @escaping DataTaskResponseValues) {
//        self.url = url
//        self.session = session as! URLSession
//        self.completionHandler = completionHandler
//    }
//    
//    
//    private var task: URLSessionDataTask?
//    
//    
//    var hasErrored: Bool {
//        return task?.error != nil
//    }
//    
//    // MARK: Overrides
//    
//    override func start() {
//        super.start()
//        
//        task = session.dataTask(with: url) { [weak self] (data, response, error) in
//            guard let strongSelf = self else { return }
//            strongSelf.finish()
//            strongSelf.completionHandler(data, response, error)
//        }
//        
//        task!.resume()
//    }
//    
//    
//    override var name: String? {
//        get {
//            return url.absoluteString
//        } set {
//            super.name = name
//        }
//    }
//    
//    
//    override func cancel() {
//        super.cancel()
//        task?.cancel()
//    }
//}

