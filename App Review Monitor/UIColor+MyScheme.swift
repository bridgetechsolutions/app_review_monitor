//
//  UIColor+MyScheme.swift
//  App Review Monitor
//
//  Created by Mark Bridges on 20/12/2016.
//  Copyright © 2016 Mark Bridges. All rights reserved.
//

import UIKit


extension UIColor {
    
    class var star: UIColor {
        return UIColor(red:0.95, green:0.60, blue:0.22, alpha:1.00)
    }
    
    class var selectedCell: UIColor {
        return UIColor(red:0.94, green:0.94, blue:0.95, alpha:1.00)
    }
    
    class var activityTopLabel: UIColor {
        return UIColor(red:0.00, green:0.00, blue:0.00, alpha:1.00)
    }
    
    class var activityBottomLabel: UIColor {
        return UIColor(red:0.56, green:0.56, blue:0.57, alpha:1.00)
    }
    
}
