//
//  SubscriptionManager.swift
//  PC HUD
//
//  Created by Mark Bridges on 18/09/2016.
//  Copyright © 2016 Mark Bridges. All rights reserved.
//

import Foundation
import SwiftyStoreKit
import StoreKit
import Crashlytics
import FirebaseRemoteConfig


class InAppSubscriptionManager {

    enum SubscriptionState: Int {
        case unknown
        case freeTrialNotStarted
        case freeTrialStarted
        case freeTrialExpired
        case subscriptionExpired
        case subscribed
    }
    
    enum VerifyReceiptResult: Int {
        case notPurchased
        case subscriptionExpired
        case subscribed
    }

    enum PurchasingError: Error {
        case unableToDownloadProduct
        case libraryError(PurchaseError)
        case unableToRestore
    }

    enum Result {
        case success
        case failure(error: Error)
    }
    
    enum SubscriptionStateUpdateResult {
        case success(subscriptionState: SubscriptionState)
        case failure(error: Error)
    }

    private(set) var product: SKProduct?
    private let productID = "com.bridgetech.app.review.monitor.all.apps"
    private let sharedSecret = "9c4dc72c568d4a96b175b5b93a4ecf57"
    

    static let subscriptionStateChangedNotificationName = Notification.Name("subscriptionStateChanged")

    var currentSubscriptionState: SubscriptionState = .unknown {
        didSet {
            NotificationCenter.default.post(name: InAppSubscriptionManager.subscriptionStateChangedNotificationName, object: SubscriptionState.RawValue())
        }
    }


    init() {
        downloadProduct(completion: nil)
        completeTransactions()
    }
    
    
    func updateSubscriptionState(withCompletion completion: @escaping (SubscriptionStateUpdateResult) -> (Void)) {
        
        getSubscriptionState { [weak self] result in
            
            switch result {
            case .success(let subscriptionState):
                self?.currentSubscriptionState = subscriptionState
                
            case .failure:
                self?.currentSubscriptionState = .unknown
            }
            
            completion(result)
        }
        
    }
    
    
    private func getSubscriptionState(withCompletion completion: @escaping (SubscriptionStateUpdateResult) -> (Void)) {
        verifySubscription { result in
            switch result {
            case .subscribed:
                completion(.success(subscriptionState: .subscribed))
                
            case .subscriptionExpired:
                completion(.success(subscriptionState: .subscriptionExpired))
                
            case .notPurchased:
                User.currentUser { result in
                    switch result {
                    case .success(let user):
                        if let dateFirstAppAdded = user.dateFirstAppAdded {
                            let elapsedTrialDuration = Date().timeIntervalSince(dateFirstAppAdded)
                            log("Trial has been running for \(elapsedTrialDuration)")
                            if elapsedTrialDuration < FIRRemoteConfig.remoteConfig().trialPeriod {
                                completion(.success(subscriptionState: .freeTrialStarted))
                            } else {
                                completion(.success(subscriptionState: .freeTrialExpired))
                            }
                            
                        } else {
                            completion(.success(subscriptionState: .freeTrialNotStarted))
                        }
                        
                    case .failure(let error):
                        completion(.failure(error: error))

                    }
                    
                }
            }
        }
    }

    func completeTransactions() {

        SwiftyStoreKit.completeTransactions() { [weak self] products in
            
            products.forEach { product in
                
                if product.transaction.transactionState == .purchased || product.transaction.transactionState == .restored {
                    log("Purchase completed \(product)")
                    self?.currentSubscriptionState = .subscribed
                }
                
            }
        }
    }

    func downloadProduct(completion: ((_ product: SKProduct?) -> Void)?) {
        SwiftyStoreKit.retrieveProductsInfo([productID]) { result in
            if let product = result.retrievedProducts.first {
                self.product = product
                log("Product downloaded: \(product.localizedDescription)")
            }
            else if let _ = result.invalidProductIDs.first {
                log("Invalid product Ids: \(result.invalidProductIDs)")
            }
            else {
                log("Error: \(result.error)")
            }

            completion?(self.product)
        }
    }

    func purchaseSubscription(purchaseCompletion: @escaping ((Result) -> Void)) {

        func purchase(product: SKProduct, innerCompletion: @escaping ((Result) -> Void)) {
            SwiftyStoreKit.purchaseProduct(product.productIdentifier) { result in
                switch result {
                case .success(_):
                    self.currentSubscriptionState = SubscriptionState.subscribed
                    innerCompletion(Result.success)

                case .error(let error):
                    // For some reason the libary doesn't return an error conforming to ErrorType
                    let properError = PurchasingError.libraryError(error)
                    innerCompletion(Result.failure(error: properError))
                }
            }
        }

        if let product = self.product {
            purchase(product: product, innerCompletion: purchaseCompletion)
        } else {
            downloadProduct(completion: { product in
                guard let product = product else {
                    purchaseCompletion(Result.failure(error: PurchasingError.unableToDownloadProduct))
                    return
                }
                purchase(product: product, innerCompletion: purchaseCompletion)
            })
        }
    }

    func restoreSubscription(_ completion: @escaping (Result) -> ()) {
        SwiftyStoreKit.restorePurchases{ results in
            
            let restoredProductMatchingOurProductID = results.restoredProducts.filter({ product in
                return product.productId == self.productID
            }).first
            
            guard restoredProductMatchingOurProductID != nil else {

                if results.restoreFailedProducts.count > 0 {
                    log("Restore Failed: \(results.restoreFailedProducts)")
                }
                else {
                    log("Nothing to Restore")
                }

                completion(.failure(error: PurchasingError.unableToRestore))
                return
            }

            log("Restore Success: \(results.restoredProducts)")
            self.currentSubscriptionState = SubscriptionState.subscribed
        }
    }


    func verifySubscription(withCompletion completion: @escaping ((VerifyReceiptResult) -> (Void))) {
        
        let appleValidator = AppleReceiptValidator(service: .production)
        
        SwiftyStoreKit.verifyReceipt(using: appleValidator, password: sharedSecret, completion: { result in
            switch result {
            case .success(let receipt):

                // Verify the purchase of a Subscription
                let purchaseResult = SwiftyStoreKit.verifySubscription(
                    productId: self.productID,
                    inReceipt: receipt,
                    validDuration: FIRRemoteConfig.remoteConfig().subscriptionDuration
                )
                switch purchaseResult {
                case .purchased(let expiresDate):
                    log("Product is valid until \(expiresDate)")
                    completion(.subscribed)
                    
                case .expired(let expiresDate):
                    log("Product is expired since \(expiresDate)")
                    completion(.subscriptionExpired)
                    
                case .notPurchased:
                    log("The user has never purchased this product")
                    completion(.notPurchased)

                }

            case .error(let error):

                print("Receipt verification failed: \(error)")

                switch error {
                case .noReceiptData:
                    SwiftyStoreKit.refreshReceipt(completion: { result in
                        switch result {
                        case .success:
                            self.verifySubscription(withCompletion: completion)

                        default:
                            break
                        }

                    })

                default:
                    break
                }

            }
        })
    }
}
