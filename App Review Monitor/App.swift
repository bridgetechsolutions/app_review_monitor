//
//  App.swift
//  App Review Monitor
//
//  Created by Mark Bridges on 10/12/2016.
//  Copyright © 2016 Mark Bridges. All rights reserved.
//

import Foundation
import CloudKit


struct App {
    
    enum Store: Int {
        case iPhone
        case iPad
        case mac
    }
    
    let id: String
    let name: String
    let iconImageURL: URL
    let store: Store
    let countries: Set<Country>
    let subscriptionID: String?
    let lastReviewsUpdate: Date?
}


// MARK: CloudSyncable

extension App: CloudSyncable {
    
    init?(record: CKRecord) {
        
        guard
            let name = record.object(forKey: "name") as? String,
            let countryIDs = record.object(forKey: "countryIDs") as? [String],
            let iconImageURLString = record.object(forKey: "iconImageURLString") as? String,
            let storeTypeRawValue = record.object(forKey: "storeType") as? Int,
            let iconImageURL = URL(string: iconImageURLString),
            let store = Store(rawValue: storeTypeRawValue) else {
                return nil
        }
        
        let countriesBuilder = CountriesListBuilder()
        
        self.id = record.recordID.recordName
        self.name = name
        self.store = store
        self.iconImageURL = iconImageURL
        self.countries = countriesBuilder.countries(forIDs: countryIDs)
        self.subscriptionID = record.object(forKey: "subscriptionID") as? String
        self.lastReviewsUpdate = record.object(forKey: "lastReviewsUpdate") as? Date
    }
    
    var record: CKRecord {
        let myRecord = CKRecord(recordType: App.recordName, recordID: recordID)
        
        myRecord.setObject(name as CKRecordValue?, forKey: "name")
        myRecord.setObject(store.rawValue as CKRecordValue?, forKey: "storeType")
        myRecord.setObject(iconImageURL.absoluteString as CKRecordValue?, forKey: "iconImageURLString")
        myRecord.setObject(countries.map{$0.id} as CKRecordValue?, forKey: "countryIDs")
        if let subscriptionID = subscriptionID {
            myRecord.setObject(subscriptionID as CKRecordValue?, forKey: "subscriptionID")
        }
        if let lastReviewsUpdate = lastReviewsUpdate {
            myRecord.setObject(lastReviewsUpdate as CKRecordValue?, forKey: "lastReviewsUpdate")
        }
        
        return myRecord
    }
    
    static var recordName: String {
        return "App"
    }
    
    var recordID: CKRecordID {
        return CKRecordID(recordName: id)
    }
    
    static var database: CKDatabase {
        let container = CKContainer.default()
        return container.privateCloudDatabase
    }
    
}


// MARK: Subscriptions

enum SaveAndSubscribeResult {
    case success(appWithSubscription: App)
    case failure(error: Error)
}

enum SubscriptionResult {
    case success(subscriptionID: String)
    case failure(error: Error)
}

enum DeleteAppResult {
    case success
    case failure(error: Error)
}


extension App {
    
    func subscribeToUpdatesAndSave(withCompletionHandler completionHandler: @escaping ((SaveAndSubscribeResult) -> (Void))) {
        
        subscribeToReviewUpdates { result in
            
            switch result {
            case .success(let subscriptionID):
                let appWithSubscription = App(id: self.id,
                                              name: self.name,
                                              iconImageURL: self.iconImageURL,
                                              store: self.store,
                                              countries: self.countries,
                                              subscriptionID: subscriptionID,
                                              lastReviewsUpdate: self.lastReviewsUpdate)
                
                appWithSubscription.save { result in
                    switch result {
                    case .success:
                        completionHandler(.success(appWithSubscription: appWithSubscription))
                        
                    case .failure(let error):
                        completionHandler(.failure(error: error))

                    }
                    
                }
                
            case .failure(let error):
                completionHandler(.failure(error: error))
            }
        }
    }
    
    func deleteAndUnsubscribe(withCompletionHandler completionHandler: @escaping ((CloudSyncResult) -> (Void))) {
        
        deleteReviewUpdatesSubscription { result in
            
            switch result {
            case .success:
                self.delete(withCompletionHandler: completionHandler)
                
            case .failure(let error):
                completionHandler(.failure(error: error))
            }
            
        }
    }
    
    private func subscribeToReviewUpdates(withCompletionHandler completionHandler: @escaping ((SubscriptionResult) -> (Void))) {
        
        let predicate = NSPredicate(format:"appID == %@", id)
        let subscription = CKQuerySubscription(recordType: FullReview.recordName, predicate: predicate, options: .firesOnRecordCreation)
        
        let notificationBuilder = NotificationBuilder()
        
        subscription.notificationInfo = notificationBuilder.cloudKitNotificationInfo(forApp: self)
        
        FullReview.database.save(subscription) { result, error in
            if let error = error {
                completionHandler(.failure(error: error))
                return
            }
            
            guard let subscriptionID = result?.subscriptionID else { fatalError("No error or subscriptionID") }
            
            completionHandler(.success(subscriptionID: subscriptionID))
        }
    }
    
    private func deleteReviewUpdatesSubscription(withCompletionHandler completionHandler: @escaping ((DeleteAppResult) -> (Void))) {
        
        guard let subscriptionID = self.subscriptionID else { fatalError("Trying to delete a subscription, when no subscriptionID exists") }
        
        FullReview.database.delete(withSubscriptionID: subscriptionID) { _, error in
            if let error = error {
                completionHandler(.failure(error: error))
            } else {
                completionHandler(.success)
            }
        }
    }
    
}


// MARK: Hashable

extension App: Hashable {
    
    public var hashValue: Int {
        return id.hashValue
    }
    
}


// MARK: Equatable

extension App: Equatable {
    
}


func ==(lhs: App, rhs: App) -> Bool {
    return lhs.hashValue == rhs.hashValue
}


// MARK: CustomStringConvertible

extension App : CustomStringConvertible {
    
    var description: String {
        return  "App(id: \(id), name: \(name))"
    }
}
