//
//  InAppSubscriptionPromptHandler.swift
//  App Review Monitor
//
//  Created by Mark Bridges on 25/12/2016.
//  Copyright © 2016 Mark Bridges. All rights reserved.
//

import UIKit
import UserNotifications
import Firebase
import MBProgressHUD


class InAppSubscriptionPromptHandler {
    
    enum ShouldBackgroundFetchResult {
        case fetchAllowed
        case fetchNotAllowed
        case fetchStateUnknown
    }
    
    let inAppSubscriptionsManager = InAppSubscriptionManager()
    private let userDefaults = UserDefaults.standard
    private let center = UNUserNotificationCenter.current()
    
    lazy private var durationFormatter: DateComponentsFormatter = {
        let formatter = DateComponentsFormatter()
        formatter.unitsStyle = .full
        formatter.includesApproximationPhrase = false
        formatter.includesTimeRemainingPhrase = false
        formatter.maximumUnitCount = 1
        return formatter
    } ()
    
    
    private let appClosedPromptShownDateKey = "appClosedPromptShownDateKey"
    private let lastBackgroundFetchPromptDateKey = "lastBackgroundFetchPromptDateKey"
    
    
    func doAppClosedCheckPrompt(withCompletionHandler completionHandler: @escaping ((GenericResult) -> (Void))) {
        
        guard userDefaults.object(forKey: appClosedPromptShownDateKey) == nil else {
            return
        }
        
        inAppSubscriptionsManager.updateSubscriptionState { [weak self] result in
            
            guard let strongSelf = self else { return }
            
            switch result {
            case .success(let subscriptionState):
                
                switch subscriptionState {
                case .freeTrialStarted:
                    
                    let durationString = strongSelf.durationFormatter.string(from: FIRRemoteConfig.remoteConfig().trialPeriod) ?? ""
                    
                    strongSelf.presentNotification(
                        withTitle: L10n.subscriptionNotificationTrialStartedTitle.string,
                        body: L10n.subscriptionNotificationTrialStartedMessage(durationString).string)
                    
                    strongSelf.userDefaults.set(Date(), forKey: strongSelf.appClosedPromptShownDateKey)
                    
                default:
                    break
                }
                
                completionHandler(.success)
                
            case .failure(let error):
                log("Error fetching user: \(error)")
                completionHandler(.failure(error: error))
            }
        }
        
    }
    
    
    func doBackgroundFetchCheckPrompt(withCompletion completion: @escaping ((ShouldBackgroundFetchResult) -> (Void))) {
        
        let remoteConfig = FIRRemoteConfig.remoteConfig()
        
        var shouldSupressVisiblePrompt = false
        
        if let lastBackgroundFetchPromptDate = userDefaults.object(forKey: lastBackgroundFetchPromptDateKey) as? Date {
            let intervalSinceLastDate = Date().timeIntervalSince(lastBackgroundFetchPromptDate)
            if intervalSinceLastDate < remoteConfig.minimumPromptToSubscribeInterval {
                shouldSupressVisiblePrompt = true
            }
        }
        
        inAppSubscriptionsManager.updateSubscriptionState { [weak self] result in
            
            guard let strongSelf = self else { return }
            
            switch result {
            case .success(let subscriptionState):
                switch subscriptionState {
                case .subscribed, .freeTrialStarted:
                    completion(.fetchAllowed)
                    
                case .freeTrialExpired:
                    if shouldSupressVisiblePrompt == false {
                        strongSelf.presentNotification(withTitle: L10n.subscriptionNotificationTrialExpiredTitle.string,
                                                       body: L10n.subscriptionNotificationTrialExpiredMessage.string)
                        strongSelf.userDefaults.set(Date(), forKey: strongSelf.lastBackgroundFetchPromptDateKey)
                    }
                    
                    completion(.fetchNotAllowed)
                    
                case .subscriptionExpired:
                    if shouldSupressVisiblePrompt == false {
                        strongSelf.presentNotification(withTitle: L10n.subscriptionNotificationSubscriptionExpiredTitle.string,
                                                       body: L10n.subscriptionNotificationSubscriptionExpiredMessage.string)
                        strongSelf.userDefaults.set(Date(), forKey: strongSelf.lastBackgroundFetchPromptDateKey)
                    }
                    completion(.fetchNotAllowed)
                    
                default:
                    break
                }
                
            case .failure(let error):
                log("Failed to update subscription state with error: \(error)")
                completion(.fetchStateUnknown)
            }
        }
        
    }
    
    
    func doAppStartCheckPrompt(in viewController: UIViewController) {
        
        func presentSubscribeAlert(withTitle title: String, message: String) {
            
            DispatchQueue.main.async {
                
                let subscriptionDurationString = self.durationFormatter.string(from: FIRRemoteConfig.remoteConfig().subscriptionDuration) ?? ""
                let productPriceString = self.inAppSubscriptionsManager.product?.localizedPrice ?? ""
                
                let alertController = UIAlertController(title: title ,
                                                        message: message,
                                                        preferredStyle: .alert)
                alertController.addAction(UIAlertAction(title: L10n.subscriptionAlertButtonTitleSubscribe(subscriptionDurationString, productPriceString).string,
                                                        style: .default,
                                                        handler: { [weak self] _ in
                                                            
                                                            EventLogger.logEvent(named: "subscription_expired_option_selected", parameters: ["selected_option" : "subscribe"])
                                                            
                                                            let progressHUD = MBProgressHUD.showAdded(to: viewController.view, animated: true)
                                                            progressHUD.label.text = L10n.subscriptionActivitySubscribing.string
                                                            progressHUD.removeFromSuperViewOnHide = true
                                                            
                                                            self?.inAppSubscriptionsManager.purchaseSubscription { result in
                                                                
                                                                DispatchQueue.main.async {
                                                                    
                                                                    let didSucceed: Bool
                                                                    
                                                                    switch result {
                                                                    case .success:
                                                                        didSucceed = true
                                                                        progressHUD.hide(animated: true)
                                                                        
                                                                    case .failure(let error):
                                                                        didSucceed = false
                                                                        progressHUD.hide(animated: true)
                                                                        let alertController = UIAlertController(error: error)
                                                                        viewController.present(alertController, animated: true, completion: nil)
                                                                    }
                                                                    
                                                                    guard let product = self?.inAppSubscriptionsManager.product else { return }
                                                                    
                                                                    EventLogger.logPurchase(withPrice: product.price,
                                                                                            currency: product.priceLocale.currencyCode ?? "",
                                                                                            success: didSucceed,
                                                                                            itemId: product.productIdentifier)
                                                                }
                                                                
                                                            }
                                                            
                }))
                alertController.addAction(UIAlertAction(title: L10n.subscriptionAlertButtonTitleRestore.string,
                                                        style: .default,
                                                        handler: { [weak self] _ in
                                                            
                                                            EventLogger.logEvent(named: "subscription_expired_option_selected", parameters: ["selected_option" : "restore"])
                                                            
                                                            let progressHUD = MBProgressHUD.showAdded(to: viewController.view, animated: true)
                                                            progressHUD.label.text = L10n.subscriptionActivityRestoring.string
                                                            progressHUD.removeFromSuperViewOnHide = true
                                                            
                                                            self?.inAppSubscriptionsManager.restoreSubscription { result in
                                                                
                                                                DispatchQueue.main.async {
                                                                    
                                                                    switch result {
                                                                    case .success:
                                                                        progressHUD.hide(animated: true)
                                                                        
                                                                    case .failure(let error):
                                                                        progressHUD.hide(animated: true)
                                                                        let alertController = UIAlertController(error: error)
                                                                        viewController.present(alertController, animated: true, completion: nil)
                                                                    }
                                                                    
                                                                }
                                                                
                                                            }
                                                            
                }))
                alertController.addAction(UIAlertAction(title: L10n.subscriptionAlertButtonTitleLater.string,
                                                        style: .destructive,
                                                        handler: { _ in
                                                            EventLogger.logEvent(named: "subscription_expired_option_selected", parameters: ["selected_option" : "later"])
                }))
                
                viewController.present(alertController, animated: true, completion: nil)
            }
        }
        
        inAppSubscriptionsManager.updateSubscriptionState { result in
            
            switch result {
            case .success(let subscriptionState):
                switch subscriptionState {
                    
                case .freeTrialExpired:
                    presentSubscribeAlert(withTitle: L10n.subscriptionAlertTrialExpiredTitle.string, message: L10n.subscriptionAlertTrialExpiredMessage.string)
                    
                case .subscriptionExpired:
                    presentSubscribeAlert(withTitle: L10n.subscriptionNotificationSubscriptionExpiredTitle.string, message: L10n.subscriptionNotificationSubscriptionExpiredMessage.string)
                    
                default:
                    break
                }
                
            case .failure(let error):
                log("Failed to update subscription state with error: \(error)")
            }
        }
    }
    
    
    private func presentNotification(withTitle title: String, body: String) {
        
        let trigger = UNTimeIntervalNotificationTrigger(timeInterval: 1, repeats: false)
        
        let content = UNMutableNotificationContent()
        content.title = title
        content.body = body
        content.sound = UNNotificationSound.default()
        content.categoryIdentifier = NotificationCategory.subscriptionPrompt.rawValue
        
        let request = UNNotificationRequest(identifier: UUID().uuidString, content: content, trigger: trigger)
        
        center.add(request)
    }
    
}
