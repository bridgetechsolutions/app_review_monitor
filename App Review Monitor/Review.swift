//
//  Review+Additions.swift
//  App Review Monitor
//
//  Created by Mark Bridges on 10/12/2016.
//  Copyright © 2016 Mark Bridges. All rights reserved.
//

import Foundation
import CloudKit


protocol Review {
    
    var id: String { get }
    var title: String { get }
    var content: String { get }
    var rating: Float { get }
}


struct FullReview: Review {
    
    let author: String
    let content: String
    let id: String
    let title: String
    let upvotes: Int
    let version: String
    let rating: Float
    let country: Country
    let appID: String
    let date: Date
}

/// Reviews from push notifications don't have all the data, so we have to use this partial review
struct PartialReview: Review {
    
    let id: String
    let title: String
    let content: String
    let rating: Float

}


// MARK: CloudSyncable

extension FullReview: CloudSyncable {
    
    init?(record: CKRecord) {
        
        let countriesListBuilder = CountriesListBuilder()
        
        guard
            let author = record.object(forKey: "author") as? String,
            let content = record.object(forKey: "content") as? String,
            let title = record.object(forKey: "title") as? String,
            let upvotes = record.object(forKey: "upvotes") as? Int,
            let version = record.object(forKey: "version") as? String,
            let rating = record.object(forKey: "rating") as? Float,
            let countryID = record.object(forKey: "countryID") as? String,
            let appID = record.object(forKey: "appID") as? String,
            let date = record.object(forKey: "date") as? Date,
            let country = countriesListBuilder.country(forID: countryID) else {
                return nil
        }
        
        self.id = record.recordID.recordName
        self.author = author
        self.content = content
        self.title = title
        self.upvotes = upvotes
        self.version = version
        self.rating = rating
        self.country = country
        self.appID = appID
        self.date = date
    }
    
    var record: CKRecord {
        let myRecord = CKRecord(recordType: FullReview.recordName, recordID: recordID)
        
        myRecord.setObject(author as CKRecordValue?, forKey: "author")
        myRecord.setObject(content as CKRecordValue?, forKey: "content")
        myRecord.setObject(title as CKRecordValue?, forKey: "title")
        myRecord.setObject(upvotes as CKRecordValue?, forKey: "upvotes")
        myRecord.setObject(version as CKRecordValue?, forKey: "version")
        myRecord.setObject(rating as CKRecordValue?, forKey: "rating")
        myRecord.setObject(country.id as CKRecordValue?, forKey: "countryID")
        myRecord.setObject(appID as CKRecordValue?, forKey: "appID")
        myRecord.setObject(date as CKRecordValue?, forKey: "date")
        
        return myRecord
    }
    
    static var recordName: String {
        return "Review"
    }
    
    var recordID: CKRecordID {
        return CKRecordID(recordName: id)
    }
    
    static var database: CKDatabase {
        let container = CKContainer.default()
        return container.publicCloudDatabase
    }
    
}


// MARK: Hashable

extension FullReview: Hashable {
    
    public var hashValue: Int {
        return id.hashValue
    }
    
}


// MARK: Equatable

extension FullReview: Equatable {
    
}


func ==(lhs: FullReview, rhs: FullReview) -> Bool {
    return lhs.hashValue == rhs.hashValue
}


// MARK: CustomStringConvertible

extension FullReview : CustomStringConvertible {
    
    var description: String {
        return  "Review(id: \(id), appID: \(appID), title: \(title))"
    }
}


// MARK: DictionaryRepresentation

extension FullReview: DictionaryRepresentable {
    
    init?(dictionaryRepresentation : [String : Any]) {
        
        guard
            let author = dictionaryRepresentation["author"] as? String,
            let content = dictionaryRepresentation["content"] as? String,
            let id = dictionaryRepresentation["id"] as? String,
            let title = dictionaryRepresentation["title"] as? String,
            let upvotes = dictionaryRepresentation["upvotes"] as? Int,
            let version = dictionaryRepresentation["version"] as? String,
            let rating = dictionaryRepresentation["rating"] as? Float,
            let countryDictionary = dictionaryRepresentation["country"] as? [String : Any],
            let appID = dictionaryRepresentation["appID"] as? String,
            let dateInterval = dictionaryRepresentation["date"] as? TimeInterval,
            let country = Country(dictionaryRepresentation: countryDictionary) else {
                return nil
        }
        
        self.init(author: author,
                  content: content,
                  id: id,
                  title: title,
                  upvotes: upvotes,
                  version: version,
                  rating: rating,
                  country: country,
                  appID: appID,
                  date: Date(timeIntervalSince1970: dateInterval))
    }
    
    var dictionaryRepresentation: [String : Any] {
        
        return [
            "author": author,
            "content": content,
            "id": id,
            "title": title,
            "upvotes": upvotes,
            "version": version,
            "rating": rating,
            "country": country.dictionaryRepresentation,
            "appID": appID,
            "date": date.timeIntervalSince1970,
        ]
        
        
    }
}
