//
//  SubscriptionsManager.swift
//  App Review Monitor
//
//  Created by Mark Bridges on 15/12/2016.
//  Copyright © 2016 Mark Bridges. All rights reserved.
//

import Foundation
import CloudKit


class SubscriptionsManager {
    
    
    enum SubscriptionsManagerError: Error {
        case failedToDeleteAllSubscriptions(errors: [Error])
    }
    
    
    private let appsFetcher = AppsFetcher()
    
    
    func updateSubscriptions(withCompletionHandler completionHandler: @escaping ((GenericResult) -> (Void))) {
        
        appsFetcher.fetchAllApps(with: .name, completionHandler: { [weak self] result in
            
            switch result {
            case .success(let apps):
                
                self?.deleteAllSubscriptions(withCompletionHandler: { result in
                    switch result {
                    case .success:
                        self?.subscribe(toApps: apps, completionHandler: completionHandler)
                        
                    case .failure(let error):
                        completionHandler(.failure(error: error))
                    }
                })
                
            case .failure(let error):
                completionHandler(.failure(error: error))
            }
            
        })
    }
    
    
    func subscribe(toApps apps: [App], completionHandler: @escaping ((GenericResult) -> (Void))) {
        
        guard apps.isEmpty == false else {
            completionHandler(.success)
            return
        }
        
        let queue = DispatchQueue.global()
        let group = DispatchGroup()
        var subscriptionCreationErrors = [Error]()
        
        apps.forEach { app in
            
            let predicate = NSPredicate(format:"appID == %@", app.id)
            let subscription = CKQuerySubscription(recordType: FullReview.recordName, predicate: predicate, options: .firesOnRecordCreation)
            
            let notification = CKNotificationInfo()
            notification.alertBody = L10n.notificationNewReviewTitle(app.name).string
//            notification.soundName = "default"
            notification.shouldBadge = true
            
            subscription.notificationInfo = notification
            
            group.enter()
            FullReview.database.save(subscription) { result, error in
                if let error = error {
                    subscriptionCreationErrors.append(error)
                }
                group.leave()
            }
        }
        
        group.notify(queue: queue, execute: {
            guard subscriptionCreationErrors.isEmpty else {
                completionHandler(.failure(error: SubscriptionsManagerError.failedToDeleteAllSubscriptions(errors: subscriptionCreationErrors)))
                return
            }
            completionHandler(.success)
        })
    }
    
    
    func deleteAllSubscriptions(withCompletionHandler completionHandler: @escaping ((GenericResult) -> (Void))) {
        
        FullReview.database.fetchAllSubscriptions { subscriptions, error in
            
            if let error = error {
                completionHandler(.failure(error: error))
                return
            }
            
            guard
                let subscriptions = subscriptions,
                subscriptions.isEmpty == false else {
                    completionHandler(.success)
                    return
            }
            

            let group = DispatchGroup()
            var subscriptionDeletionErrors = [Error]()
            
            subscriptions.forEach { subscription in
                group.enter()
                FullReview.database.delete(withSubscriptionID: subscription.subscriptionID) { _, error in
                    if let error = error {
                        subscriptionDeletionErrors.append(error)
                    }
                    group.leave()
                }
            }
            
            group.notify(queue: .global(), execute: {
                guard subscriptionDeletionErrors.isEmpty else {
                    completionHandler(.failure(error: SubscriptionsManagerError.failedToDeleteAllSubscriptions(errors: subscriptionDeletionErrors)))
                    return
                }
                completionHandler(.success)
            })
            
        }
    }
    
}
