//
//  AsynchronousOperation.swift
//  App Review Monitor
//
//  Created by Mark Bridges on 11/12/2016.
//  Copyright © 2016 Mark Bridges. All rights reserved.
//

import Foundation


class AsynchronousOperation: Operation {
    
    enum OperationStatus {
        case notStarted
        case executing
        case cancelled
        case failed(error: Error)
        case succeeded(result: Any?)
    }
    
    
    var operationStatus: OperationStatus = .notStarted {
        willSet {
            willChangeValue(forKey: #keyPath(isCancelled))
            willChangeValue(forKey: #keyPath(isExecuting))
            willChangeValue(forKey: #keyPath(isFinished))
        }
        didSet {
            didChangeValue(forKey: #keyPath(isCancelled))
            didChangeValue(forKey: #keyPath(isExecuting))
            didChangeValue(forKey: #keyPath(isFinished))
        }
    }
    
    
    // MARK: Overrides
    
    override func start() {
        super.start()
        
        operationStatus = .executing
    }
    
    
    override func cancel() {
        super.cancel()
        
        operationStatus = .cancelled
    }
    
    
    override var isCancelled: Bool {
        
        switch operationStatus {
        case .cancelled:
            return true
            
        default:
            return false
        }
    }
    
    
    override var isAsynchronous: Bool {
        return true
    }
    
    
    override var isExecuting: Bool {
        
        switch operationStatus {
        case .executing:
            return true
            
        default:
            return false
        }
    }
    
    
    override var isFinished: Bool {
        
        switch operationStatus {
        case .failed(_), .succeeded(_):
            return true
            
        default:
            return false
        }
    }
    
    
//    func finishWithSuccess(withResult result: Any?) {
//        
//        completionBlock?()
//    }
}
