//
//  ReviewView.swift
//  App Review Monitor
//
//  Created by Mark Bridges on 14/12/2016.
//  Copyright © 2016 Mark Bridges. All rights reserved.
//

import UIKit


class ReviewView: UIView {
    
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var starRatingView: StarRatingView!
    @IBOutlet weak var authorAndCountryLabel: UILabel!
    @IBOutlet weak var dateLabel: UILabel!
    @IBOutlet weak var reviewContentLabel: UILabel!
    

    var review: Review? = nil {
        didSet {
            refresh()
        }
    }
    
    
    private func refresh() {
        titleLabel.text = review?.title
        starRatingView.rating = review?.rating ?? 0
        reviewContentLabel.text = review?.content

        if let fullReview = review as? FullReview {
            authorAndCountryLabel.isHidden = false
            dateLabel.isHidden = false
            authorAndCountryLabel.text = fullReview.authorAndCountryDisplayString
            dateLabel.text = fullReview.dateString
        } else {
            authorAndCountryLabel.isHidden = true
            dateLabel.isHidden = true
            authorAndCountryLabel.text = nil
            dateLabel.text = nil
        }
    }
    
}


private let displayDateFormatter: DateFormatter = {
    let dateFormatter = DateFormatter()
    dateFormatter.dateStyle = .medium
    dateFormatter.locale = Locale(identifier: "en_US_POSIX")
    return dateFormatter
}()


extension FullReview {
    
    var authorAndCountryDisplayString: String {
        return L10n.reviewAuthorAndCountry(author, country.name).string
    }
    
    var dateString: String {
        return displayDateFormatter.string(from: date)
    }
}
