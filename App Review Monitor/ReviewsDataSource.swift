//
//  ReviewsViewControllerDataSource.swift
//  App Review Monitor
//
//  Created by Mark Bridges on 15/12/2016.
//  Copyright © 2016 Mark Bridges. All rights reserved.
//

import UIKit
import CloudKit


class ReviewsDataSource: NSObject {
    
    enum FetchState {
        case noResultsFetched
        case someResultsFetched(cursor: CKQueryCursor)
        case allResultsFetched
    }
    
    let app: App
    private(set) var reviewsTable = ReviewsTable()
    
    var fetchState: FetchState {
        if reviewsTable.isEmpty {
            return .noResultsFetched
        }
        if let cursor = cursor {
            return .someResultsFetched(cursor: cursor)
        }
        
        return .allResultsFetched
    }
    
    private var cursor: CKQueryCursor?
    
    required init(app: App) {
        self.app = app
    }
    
    
    func fetchResults(withCompletionHandler completionHandler: @escaping ((GenericResult) -> (Void))) {
        
        switch fetchState {
        case .noResultsFetched:
            let predicate = NSPredicate(format: "appID == '\(self.app.id)'")
            let query = CKQuery(recordType: FullReview.recordName, predicate: predicate)
            
            // Removed the version sort descriptor, as it doesn't work when we deal versions like '1.12.2'
//            let versionSortDescriptor = NSSortDescriptor(key: "version", ascending: false, selector: #selector(NSString.localizedStandardCompare(_:)))
//            let dateSortDescriptor = NSSortDescriptor(key: "date", ascending: false)
//            query.sortDescriptors = [versionSortDescriptor, dateSortDescriptor]
            
            let dateSortDescriptor = NSSortDescriptor(key: "date", ascending: false)
            query.sortDescriptors = [dateSortDescriptor]
            let operation = CKQueryOperation(query: query)
            configure(operation: operation, completionHandler: completionHandler)
            OperationQueue.main.addOperation(operation)
            
        case .allResultsFetched:
            completionHandler(.success)
            break
            
        case .someResultsFetched(let cursor):
            let operation = CKQueryOperation(cursor: cursor)
            configure(operation: operation, completionHandler: completionHandler)
            OperationQueue.main.addOperation(operation)
        }
    }
    
    
    func resetResults() {
        reviewsTable = ReviewsTable()
    }
    
    
    private func configure(operation: CKQueryOperation, completionHandler: @escaping ((GenericResult) -> (Void))) {
        operation.resultsLimit = CKQueryOperationMaximumResults
        operation.database = FullReview.database
        operation.qualityOfService = .userInitiated
        operation.recordFetchedBlock = { [weak self] record in
            guard let review = FullReview(record: record) else {
                return
            }
            self?.reviewsTable.add(review: review)
        }
        operation.queryCompletionBlock = { [weak self] cursor, error in
            
            if let error = error  {
                completionHandler(.failure(error: error))
                return
            }
            
            self?.cursor = cursor
            
            completionHandler(.success)
        }
    }

}


extension ReviewsDataSource: UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return reviewsTable.numberOfSections()
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return reviewsTable.numberOfRows(inSection: section)
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: ReviewTableViewCell.identifier, for: indexPath) as! ReviewTableViewCell
        
        let review = reviewsTable.object(atIndexPath: indexPath)
        cell.reviewView.review = review
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        
        guard reviewsTable.sortedSections.count > section else {
            return nil
        }
        
        let versionSection = reviewsTable.sortedSections[section]
        return versionSection.name
    }
}


class ReviewsTable {
    
    private var sections = Set<VersionSection>()
    
    var totalRowsInAllSections: Int {
        var count = 0
        sections.forEach{ count = count + $0.reviews.count }
        return count
    }
    
    var isEmpty: Bool {
        return sections.isEmpty
    }
    
    var sortedSections: [VersionSection] {
        
        let sortDescriptor = NSSortDescriptor(key: "name", ascending: false, selector: #selector(NSString.localizedStandardCompare(_:)))
        
        let sortedSections = (Array(sections) as NSArray).sortedArray(using: [sortDescriptor]) as! [VersionSection]

        return sortedSections
    }
    
    func add(review: FullReview) {
        let sectionToAddTo: VersionSection = section(for: review) ?? VersionSection(name: review.version)
        sections.insert(sectionToAddTo)
        sectionToAddTo.add(review: review)
    }
    
    func object(atIndexPath indexPath: IndexPath) -> FullReview {
        return sortedSections[indexPath.section].sortedReviews[indexPath.row]
    }
    
    func numberOfRows(inSection section: Int) -> Int {
        guard sections.count > section else {
            return 0
        }
        
        return sortedSections[section].reviews.count
    }
    
    func numberOfSections() -> Int {
        return sections.count
    }
    
    func section(for fullReview: FullReview) -> VersionSection? {
        return sections.filter({ $0.name == fullReview.version}).first
    }
    
    func indexPath(for fullReview: FullReview) -> IndexPath? {
        guard
            let section = section(for: fullReview),
            let sectionIndex = sortedSections.index(of: section),
            let rowIndex = section.sortedReviews.index(of: fullReview) else {
                return nil
        }
        
        return IndexPath(row: rowIndex, section: sectionIndex)
    }
}


// MARK: VersionSection

class VersionSection: NSObject {
    
    let name: String
    private(set)var reviews = Set<FullReview>()
    
    required init(name: String) {
        self.name = name
    }
    
    func add(review: FullReview) {
        reviews.insert(review)
    }
    
    var sortedReviews: [FullReview] {
        return reviews.sorted { (review1, review2) -> Bool in
            return review1.date > review2.date
        }
    }
    
}


// MARK: Hashable

extension VersionSection {
    
    override public var hashValue: Int {
        return name.hashValue
    }
    
}


// MARK: Equatable

extension VersionSection {
    
    override func isEqual(_ object: Any?) -> Bool {
        guard let otherObject = object as? VersionSection else { return false }
        return name == otherObject.name
    }
    
}


func ==(lhs: VersionSection, rhs: VersionSection) -> Bool {
    return lhs.hashValue == rhs.hashValue
}

