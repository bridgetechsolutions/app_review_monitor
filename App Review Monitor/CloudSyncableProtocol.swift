//
//  CloudSyncableProtocol.swift
//  App Review Monitor
//
//  Created by Mark Bridges on 13/12/2016.
//  Copyright © 2016 Mark Bridges. All rights reserved.
//

import Foundation
import CloudKit


let cloudKitContainerIdentifier = "iCloud.com.bridgetech.App-Review-Monitor"

enum CloudSyncResult {
    case success
    case failure(error: Error)
}


protocol CloudSyncable {
    
    init?(record: CKRecord)
    
    var record: CKRecord { get }

    static var recordName: String { get }
    
    var recordID: CKRecordID { get }
    
    static var database: CKDatabase { get }
}


extension CloudSyncable {
    
//    static func fetchAllRecords(withCompletionHandler completionHandler: @escaping ((CloudSyncResult) -> (Void))) {
//        
//        let container = CKContainer.defaultContainer()
//        let privateDatabase = container.privateCloudDatabase
//        let predicate = NSPredicate(value: true)
//        
//        let query = CKQuery(recordType: "Notes", predicate: predicate)
//        
//        
//        dataBase.perform(<#T##query: CKQuery##CKQuery#>, inZoneWith: nil) { (<#[CKRecord]?#>, <#Error?#>) in
//            
//        }
//        
//    }
    
    func save(withCompletionHandler completionHandler: @escaping ((CloudSyncResult) -> (Void))) {
        
        let saveOperation = CKModifyRecordsOperation(recordsToSave: [record], recordIDsToDelete: nil)
        saveOperation.database = Self.database
        saveOperation.savePolicy = .allKeys
        saveOperation.modifyRecordsCompletionBlock = {  _, _, error in
            
            if let error = error {
                completionHandler(.failure(error: error))
                return
            }
            
            completionHandler(.success)
        }
        
        Self.database.add(saveOperation)
    }
    
    func delete(withCompletionHandler completionHandler: @escaping ((CloudSyncResult) -> (Void))) {
        
        Self.database.delete(withRecordID: recordID, completionHandler: { _, error in
            if let error = error {
                completionHandler(.failure(error: error))
                return
            }
            
            completionHandler(.success)
        })
    }
}
