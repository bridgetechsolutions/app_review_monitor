//
//  ReviewsViewController.swift
//  App Review Monitor
//
//  Created by Mark Bridges on 13/12/2016.
//  Copyright © 2016 Mark Bridges. All rights reserved.
//

import UIKit
import CloudKit
import SwiftyTimer

class ReviewsViewController: UIViewController {
    
    enum SelectingReviewError: Error {
        case unableToFindCorrespondingReview
    }
    
    @IBOutlet private var emptyPlaceholderView: UIView?
    @IBOutlet private var emptyPlaceholderTitleLabel: UILabel? {
        didSet {
            emptyPlaceholderTitleLabel?.text = L10n.reviewsEmptyStateTitle.string
        }
    }
    @IBOutlet private var emptyPlaceholderDetailLabel: UILabel? {
        didSet {
            emptyPlaceholderDetailLabel?.text = L10n.reviewsEmptyStateMessage.string
        }
    }
    
    @IBOutlet private var updateActivityStatusView: UpdateActivityStatusView?
    
    var app: App? = nil {
        didSet {
            guard let app = app else { return }
            navigationItem.title = app.name
            reviewsDataSource = ReviewsDataSource(app: app)
        }
    }
    
    fileprivate var reviewsDataSource: ReviewsDataSource?
    
    fileprivate lazy var refreshControl: UIRefreshControl = {
        let newRefreshControl = UIRefreshControl()
        newRefreshControl.addTarget(self, action: #selector(reloadReviewsFromAppStore), for: .valueChanged)
        return newRefreshControl
    } ()
    
    @IBOutlet weak var tableView: UITableView? {
        didSet {
            tableView?.register(UINib(nibName: ReviewTableViewCell.identifier, bundle: nil), forCellReuseIdentifier: ReviewTableViewCell.identifier)
            tableView?.dataSource = reviewsDataSource
            tableView?.delegate = self
            tableView?.estimatedRowHeight = 100.0
            tableView?.rowHeight = UITableViewAutomaticDimension
            tableView?.refreshControl = refreshControl
        }
    }
    
    var isEmptyPlaceholderShowing: Bool {
        get {
            return emptyPlaceholderView?.alpha == 0
        } set {
            UIView.animate(withDuration: 0.3) { [weak self] in
                self?.emptyPlaceholderView?.alpha = newValue ? 1 : 0
                self?.tableView?.alpha = newValue ? 0 : 1
            }
        }
    }
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        view.backgroundColor = UIColor.white
        
        navigationItem.title = app?.name ?? L10n.reviewsTitle.string
        
        guard app != nil else {
            updateActivityStatusView?.isLoading = false
            updateActivityStatusView?.lastUpdateDate = nil
            isEmptyPlaceholderShowing = true
            return
        }
        
        reloadReviewsFromAppStore()
        
        reloadData()
    }
    
    
    func reloadReviewsFromAppStore() {
        
        guard let app = app else {
            tableView?.isLoading = false
            updateActivityStatusView?.isLoading = false
            refreshControl.endRefreshing()
            return
        }
        
        let dataSyncer = UIApplication.dataSyncer
        updateActivityStatusView?.lastUpdateDate = dataSyncer.lastUpdateDate(forApp: app)
        
        updateActivityStatusView?.isLoading = true
        
        _ = dataSyncer.updateReviews(forApp: app, completionHandler: { [weak self]  result in
            
            DispatchQueue.main.async { [weak self] in
                
                guard let strongSelf = self else { return }
                
                strongSelf.updateActivityStatusView?.isLoading = false
                
                switch result {
                case .success(let newReviews):
                    self?.updateActivityStatusView?.lastUpdateDate = dataSyncer.lastUpdateDate(forApp: app)
                    // If new reviews have come in we should reload the table
                    if newReviews.isEmpty == false {
                        strongSelf.reviewsDataSource?.resetResults()
                        strongSelf.reloadData()
                    } else {
                        strongSelf.refreshControl.endRefreshing()
                    }
                    
                case .failure(let error):
                    let alertController = UIAlertController(error: error)
                    strongSelf.present(alertController, animated: true, completion: nil)
                }
            }
        })
        
    }
    
    
    fileprivate func reloadData(withCompletion completion: ((GenericResult) -> (Void))? = nil) {
        
        tableView?.isLoading = true
        
        reviewsDataSource?.fetchResults { [weak self] result in
            
            DispatchQueue.main.async { [weak self] in
                
                self?.refreshControl.endRefreshing()
                
                switch result {
                case .success:
                    self?.isEmptyPlaceholderShowing = self?.reviewsDataSource?.reviewsTable.isEmpty ?? false
                    self?.tableView?.reloadData()
                    
                    EventLogger.logEvent(named: "fetched_reviews", parameters: [
                        "number_of_fetched_reviews" : self?.reviewsDataSource?.reviewsTable.totalRowsInAllSections ?? 0
                        ])
                    
                    completion?(.success)

                case .failure(let error):
                    // If we're calling this with a completion, the completion handler can deal with showing the error
                    if let completion = completion {
                        completion(.failure(error: error))
                    } else {
                        let alertController = UIAlertController(error: error)
                        self?.present(alertController, animated: true, completion: nil)
                    }
                }
                
                self?.tableView?.isLoading = false
            }
            
        }
    }
    
    
    func select(fullReview: FullReview, toShare: Bool, completionHandler: @escaping ((GenericResult) -> Void)) {
        
        reloadData { [weak self] result in
            switch result {
            case .success:
                guard
                    let reviewsTable = self?.reviewsDataSource?.reviewsTable,
                    let matchingReviewIndexPath = reviewsTable.indexPath(for: fullReview) else {
                        completionHandler(.failure(error: SelectingReviewError.unableToFindCorrespondingReview))
                        return
                }
                
                self?.tableView?.selectRow(at: matchingReviewIndexPath, animated: true, scrollPosition: .middle)
                completionHandler(.success)
                
            case .failure(let error):
                completionHandler(.failure(error: error))
            }
        }
        
    }
    
    
    func share(_ review: FullReview, sourceView: UIView, length: ActivityObjectsBuilder.Length) {
        
        guard let app = app else { fatalError("Trying to share without an app") }
        
        let activityBuilder = ActivityObjectsBuilder()
        
        let activityObjects = activityBuilder.activityObjects(for: review, app: app, length: length)
        
        let activityVC = UIActivityViewController(activityItems: activityObjects, applicationActivities: nil)
        activityVC.popoverPresentationController?.sourceView = sourceView
        activityVC.completionWithItemsHandler = { [weak self] (activityType, completed, _, error) in
            
            self?.tableView?.deselectAllCells()
            
            if completed {
                EventLogger.logShareEvent(withMethodName: activityType?.rawValue ?? "", contentType: String(describing: length), contentId: review.id)
            }
        }
        present(activityVC, animated: true, completion: nil)
    }
}


extension ReviewsViewController: UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        
        guard
            let lastIndexPath = tableView.lastIndexPath,
            let fetchState = reviewsDataSource?.fetchState else {
                return
        }
        
        switch (indexPath, fetchState) {
        case (lastIndexPath, .someResultsFetched(_)):
            reloadData()
        default:
            break
        }
        
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        guard
            let review = reviewsDataSource?.reviewsTable.object(atIndexPath: indexPath),
            let cell = tableView.cellForRow(at: indexPath) else {
            return
        }
        
        let actionSheet = UIAlertController(title: L10n.shareAlertTitle.string, message: nil, preferredStyle: .actionSheet)
        actionSheet.popoverPresentationController?.sourceView = cell
        
        actionSheet.addAction(UIAlertAction(title: L10n.shareAlertOptionFull.string, style: .default, handler: { [weak self] _ in
            self?.share(review, sourceView: cell, length: .full)
        }))
        
        actionSheet.addAction(UIAlertAction(title: L10n.shareAlertOptionShort.string, style: .default, handler: { [weak self] _ in
            self?.share(review, sourceView: cell, length: .trimmed)
        }))
        
        actionSheet.addAction(UIAlertAction(title: L10n.globalCancel.string, style: .cancel, handler: { [weak self] _ in
            self?.tableView?.deselectAllCells()
        }))
        
        present(actionSheet, animated: true, completion: nil)
    }
}
