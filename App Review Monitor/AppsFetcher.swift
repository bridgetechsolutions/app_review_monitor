//
//  AppsFetcher.swift
//  App Review Monitor
//
//  Created by Mark Bridges on 15/12/2016.
//  Copyright © 2016 Mark Bridges. All rights reserved.
//

import Foundation
import CloudKit


class AppsFetcher {
    
    enum Result {
        case success(apps: [App])
        case failure(error: Error)
    }
    
    enum AppsDataSourceError: Error {
        case resultsConcatenated
    }
    
    enum SortOrder: String {
        case name = "name"
        case lastReviewsUpdate = "lastReviewsUpdate"
    }
    
    
    func fetchAllApps(with sortOrder: SortOrder, completionHandler: @escaping ((Result) -> (Void))) {
        
        var results: [App] = []
        
        let predicate = NSPredicate(value: true)
        let sortDescriptor = NSSortDescriptor(key: sortOrder.rawValue, ascending: true)
        let query = CKQuery(recordType: App.recordName, predicate: predicate)
        query.sortDescriptors = [sortDescriptor]
        let operation = CKQueryOperation(query: query)
        operation.resultsLimit = CKQueryOperationMaximumResults
        operation.database = App.database
        operation.qualityOfService = .userInitiated
        operation.recordFetchedBlock = { record in
            guard let app = App(record: record) else {
                return
            }
            results.append(app)
        }
        operation.queryCompletionBlock = { cursor, error in
            
            guard cursor == nil else {
                // TODO: Deal with pagination
                completionHandler(.failure(error: AppsDataSourceError.resultsConcatenated))
                return
            }
            
            if let error = error  {
                completionHandler(.failure(error: error))
                return
            }
            
            completionHandler(.success(apps: results))
        }
        
        OperationQueue.main.addOperation(operation)
    }
    
}
