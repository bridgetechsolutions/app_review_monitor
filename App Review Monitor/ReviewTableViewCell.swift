//
//  ReviewTableViewCell.swift
//  App Review Monitor
//
//  Created by Mark Bridges on 14/12/2016.
//  Copyright © 2016 Mark Bridges. All rights reserved.
//

import UIKit
import PureLayout


class ReviewTableViewCell: UITableViewCell {
    
    static let identifier = "ReviewTableViewCell"
    
    lazy var reviewView: ReviewView = {
        let reviewView = Bundle.main.loadNibNamed("ReviewView", owner: self, options: nil)?[0] as! ReviewView
        reviewView.translatesAutoresizingMaskIntoConstraints = false
        return reviewView
    } ()

    override func awakeFromNib() {
        super.awakeFromNib()
        
        let bgColorView = UIView()
        bgColorView.backgroundColor = UIColor.selectedCell
        selectedBackgroundView = bgColorView
        
        contentView.addSubview(reviewView)
        reviewView.autoPinEdgesToSuperviewMargins()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
