//
//  FIRRemoteConfig+Westwing.swift
//  Westwing
//
//  Created by Mark Bridges on 25/10/2016.
//  Copyright © 2016 Westwing Group GmbH. All rights reserved.
//

import Firebase


extension FIRRemoteConfig {

    var trialPeriod: TimeInterval {
        return configValue(forKey: "trialPeriod").numberValue?.doubleValue ?? 1209600
    }
    
    var minimumBackgroundFetchInterval: TimeInterval {
        return configValue(forKey: "minimumBackgroundFetchInterval").numberValue?.doubleValue ?? 36000
    }
    
    var freeAppLimitCount: Int {
        return configValue(forKey: "freeAppLimitCount").numberValue?.intValue ?? 1
    }
    
    var minimumPromptToSubscribeInterval: TimeInterval {
        return configValue(forKey: "minimumPromptToSubscribeInterval").numberValue?.doubleValue ?? 1209600
    }
    
    var subscriptionDuration: TimeInterval {
        return configValue(forKey: "subscriptionDuration").numberValue?.doubleValue ?? 31536000
    }
    
    var isAppInDebugMode: Bool {
        return configValue(forKey: "debugMode").boolValue
    }
}
