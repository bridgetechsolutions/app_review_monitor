//
//  DataSyncer.swift
//  App Review Monitor
//
//  Created by Mark Bridges on 14/12/2016.
//  Copyright © 2016 Mark Bridges. All rights reserved.
//

import Foundation
import CloudKit


protocol DataSyncerDelegate: class {
    
    func dataSyncer(_ dataSyncer: DataSyncer, didFailWithError error: Error, forApp app: App?)
    func dataSyncer(_ dataSyncer: DataSyncer, didUpdateReviews reviews: [Review], forApp app: App)
}


enum GenericResult {
    case success
    case failure(error: Error)
}


class DataSyncer {
    
    enum FetchAllAppsReviewsResult {
        case success(appsAndNewReviews: [AppWithNewReviews])
        case failure(error: Error)
    }
    
    typealias AppWithNewReviews = (app: App, newReviews: Set<FullReview>)
    typealias FetchAllAppsReviewsCompletion = ((FetchAllAppsReviewsResult) -> (Void))
    typealias EmptyCompletion = ((Void) -> (Void))
    
    enum DataSyncerError: Error {
        case appsForUserExceededMaximumResults
        case someAppsFailedToUpdateReviews(errors: [Error])
    }
    
    private let appsFetcher = AppsFetcher()    
    private let operationQueue = OperationQueue()
    

    weak var delegate: DataSyncerDelegate?
    
    var isSyncInProgress: Bool {
        let activeOperations = operationQueue.operations.filter{ operation in
            return (operation.isCancelled == false && operation.isFinished == false)
        }
        return activeOperations.count > 0
    }
    
    // Call whenever an app is updated, so we can incrementally get the new results
    var perAppUpdateBlock: ((AppWithNewReviews) -> (Void))? = nil
    
    func updateReviewsForAllApps(withCompletionHandler completionHandler: @escaping FetchAllAppsReviewsCompletion) {
        
        var appsWithNewReviews: [AppWithNewReviews] = []
        
        appsFetcher.fetchAllApps(with: .lastReviewsUpdate, completionHandler: { [weak self] result in
            
            guard let strongSelf = self else { return }
            
            switch result {
            case .success(let apps):
                
                var updateReviewsErrors = [Error]()
                
                let allDoneOperation = Operation()
                allDoneOperation.completionBlock = {
                    
                    guard updateReviewsErrors.isEmpty else {
                        completionHandler(.failure(error: DataSyncerError.someAppsFailedToUpdateReviews(errors: updateReviewsErrors)))
                        return
                    }
                    
                    completionHandler(.success(appsAndNewReviews: appsWithNewReviews))
                }
                
                apps.forEach{ app in
                    let fetchAndUploadOperation = strongSelf.updateReviews(forApp: app, isPaginationEnabled: false, completionHandler: { result in
                        switch result {
                        case .success(let newReviews):
                            
                            let appWithNewReviews: AppWithNewReviews = (app: app, newReviews: newReviews)
                            appsWithNewReviews.append(appWithNewReviews)
                            strongSelf.perAppUpdateBlock?(appWithNewReviews)
                            
                        case .failure(let error):
                            updateReviewsErrors.append(error)
                        }
                    })
                    allDoneOperation.addDependency(fetchAndUploadOperation)
                }
                strongSelf.operationQueue.addOperation(allDoneOperation)
                
            case .failure(let error):
                completionHandler(.failure(error: error))
            }
        })
    }
    
    
    func updateReviews(forApp app: App, isPaginationEnabled: Bool = true,  completionHandler: @escaping FetchAndUploadReviewsForAppOperation.FetchAndUploadReviewsCompletion) -> FetchAndUploadReviewsForAppOperation {
        if let existingOperation = runningOperation(forApp: app) {
            log("Operation already in progress to update app: \(app)")
            existingOperation.add(completionHandler: completionHandler)
            return existingOperation
        } else {
            let fetchAndUploadOperation = FetchAndUploadReviewsForAppOperation(app: app, isPaginationEnabled: isPaginationEnabled, endDate:nil, completionHandler: completionHandler)
            fetchAndUploadOperation.add(completionHandler: { [weak self] result in
                switch result {
                case .success(_):
                    self?.storeLastUpdateDate(forApp: app)

                default:
                    break
                }
            })
            operationQueue.addOperation(fetchAndUploadOperation)
            return fetchAndUploadOperation
        }
    }
    
    
    /// When all the fetch and upload operations complete this will fire
    func whenQueueCompletes(completion: @escaping EmptyCompletion) {
        
        guard runningFetchAndUploadOperations.isEmpty == false else {
            completion()
            return
        }
        
        let allDoneOperation = Operation()
        allDoneOperation.completionBlock = completion
        
        runningFetchAndUploadOperations.forEach { operation in
            allDoneOperation.addDependency(operation)
        }
        
        operationQueue.addOperation(allDoneOperation)
    }
    
    
    private func runningOperation(forApp app: App) -> FetchAndUploadReviewsForAppOperation? {
        return runningFetchAndUploadOperations.filter { $0.app == app }.first
    }
    

    private var runningFetchAndUploadOperations: [FetchAndUploadReviewsForAppOperation] {
        
        return operationQueue.operations.flatMap { operation in
            
            guard
                let operation = operation as? FetchAndUploadReviewsForAppOperation,
                operation.isFinished == false && operation.isCancelled == false else {
                    return nil
            }
            
            return operation
        }
    }
    
    
    func storeLastUpdateDate(forApp app: App) {
        UserDefaults.standard.set(Date(), forKey: "\(app.id).lastLocalReviewsUpdate")
    }

    
    func lastUpdateDate(forApp app: App) -> Date? {
        let cachedDate = UserDefaults.standard.object(forKey: "\(app.id).lastLocalReviewsUpdate") as? Date
        
        let lastReviewsUpdate = app.lastReviewsUpdate
        
        guard
            let cached = cachedDate else {
                return lastReviewsUpdate
        }
        
        guard
            let cloud = lastReviewsUpdate else {
                return cachedDate
        }
        
        if cached > cloud {
            return cached
        } else {
            return cloud
        }
        
    }
}
