//
//  ProgressProtocol.swift
//  PreviewVideoConverter
//
//  Created by Mark Bridges on 16/07/2016.
//  Copyright © 2016 BridgeTech. All rights reserved.
//

import Foundation

protocol Progressable {
    var progress: Float { get }
}
