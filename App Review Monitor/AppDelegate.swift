//
//  AppDelegate.swift
//  App Review Monitor
//
//  Created by Mark Bridges on 10/12/2016.
//  Copyright © 2016 Mark Bridges. All rights reserved.
//

import UIKit
import Fabric
import Crashlytics
import Firebase
import UserNotifications
import CloudKit
import SwiftyTimer
import AlamofireNetworkActivityIndicator
import CocoaLumberjack


@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?
        
    let dataSyncer = DataSyncer()
    let subscriptionsManager = SubscriptionsManager()
    let notificationsHandler = NotificationsHandler()
    let inAppSubscriptionPromptHandler = InAppSubscriptionPromptHandler()
    
    private var backgroundTaskIdentifier: UIBackgroundTaskIdentifier? = nil
    private var subscriptionBackgroundTaskIdentifier: UIBackgroundTaskIdentifier? = nil
    
    let shareAction = UNNotificationAction(identifier: "com.bridgetech.app-review-monitor.share", title: L10n.notificationNewReviewShareButtonTitle.string, options: .foreground)
    
    func application(_ application: UIApplication, willFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey : Any]? = nil) -> Bool {
        
        configureDependencies()
        
        FIRRemoteConfig.remoteConfig().fetch {(status, error) in
            
            log("Fetched remote config status: \(status), error: \(error)")
            
            if status == .success {
                FIRRemoteConfig.remoteConfig().activateFetched()
                UIApplication.shared.setMinimumBackgroundFetchInterval(FIRRemoteConfig.remoteConfig().minimumBackgroundFetchInterval)
            }
        }
        
        return true
    }
    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
        
//        UNUserNotificationCenter.current().requestAuthorization(options: [.alert, .sound, .badge]) { granted, error in
//
//        }
        
//        subscriptionsManager.deleteAllSubscriptions { result in
//            log("deleted subscriptions, with result \(result)")
//        }
        
        NetworkActivityIndicatorManager.shared.isEnabled = true
        
        UNUserNotificationCenter.current().delegate = self
        let newReviewCategory = UNNotificationCategory(identifier: "com.bridgetech.notification.newreview", actions: [shareAction], intentIdentifiers: [], options: [])
        UNUserNotificationCenter.current().setNotificationCategories([newReviewCategory])
        
        return true
    }

    func applicationWillResignActive(_ application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
    }

    func applicationDidEnterBackground(_ application: UIApplication) {
        
//        let country = Country(id: "gb", name: "United Kingdom")
//        let testApp = App(id: "930574573", name: "App Review Monitor", iconImageURL: URL(string: "http://www.somewhere.com")!, store: .iPhone, countries: Set([country]), subscriptionID: "12314125", lastReviewsUpdate: Date())
//        let testReview = FullReview(author: "Mark", content: "This is easily the best app review monitoring app that's ever been made!", id: "1344694354", title: "Fantastic App", upvotes: 0, version: "1.9.2", rating: 5, country: country, appID: "930574573", date: Date())
//        presentNotification(forApp: testApp, withReview: testReview)
        
        if dataSyncer.isSyncInProgress {
            log("Entering background while sync is in progress, starting background task to continue sync")
            startBackgroundTask()
            dataSyncer.whenQueueCompletes { [weak self] in
                log("Data syncer completed. Ending background task")
                self?.endBackgroundTask()
            }
        }
        
        startSubscriptionBackgroundTask()
        inAppSubscriptionPromptHandler.doAppClosedCheckPrompt { [weak self] result in
            self?.endSubscriptionBackgroundTask()
        }
    }

    func applicationWillEnterForeground(_ application: UIApplication) {
        // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
    }

    func applicationDidBecomeActive(_ application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
        
        UIApplication.shared.applicationIconBadgeNumber = 0
        
        endBackgroundTask()
        endSubscriptionBackgroundTask()
    }

    func applicationWillTerminate(_ application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
        // Saves changes in the application's managed object context before the application terminates.
    }
    
    func application(_ application: UIApplication, performFetchWithCompletionHandler completionHandler: @escaping (UIBackgroundFetchResult) -> Void) {
        
        startBackgroundTask()
                
        EventLogger.logEvent(named: "started_background_fetch")
        
        presentDebugNotification(withMessage: "Background Fetch Started")
        
        var secondsElapsed = 0
        let maxFetchTime = 29
        
        let timer = Timer.new(every: 1.second) { 
            
            secondsElapsed = secondsElapsed.advanced(by: 1)
            
            log("Background task has been running for \(secondsElapsed) seconds")

//            if secondsElapsed >= maxFetchTime {
//                log("Ran out of background fetch time")
//                completionHandler(UIBackgroundFetchResult.failed)
//            }
        }
        timer.start(modes: .defaultRunLoopMode)
        
        func endTimer() {
            log("Completed background fetch in \(secondsElapsed) seconds")
            presentDebugNotification(withMessage: "Background task finished in \(secondsElapsed) seconds")
            timer.invalidate()
            endBackgroundTask()
        }
        
        inAppSubscriptionPromptHandler.doBackgroundFetchCheckPrompt { [weak self] result in
            switch result {
            case .fetchAllowed:
                self?.dataSyncer.perAppUpdateBlock = { [weak self] appWithNewReviews in
                    appWithNewReviews.newReviews.forEach { review in
                        self?.presentNotification(forApp: appWithNewReviews.app, withReview: review)
                    }
                    UIApplication.shared.applicationIconBadgeNumber = UIApplication.shared.applicationIconBadgeNumber + appWithNewReviews.newReviews.count
                }
                
                self?.dataSyncer.updateReviewsForAllApps { result in
                    
                    switch result {
                    case .success(let appsWithNewReviews):
                        
                        var allReviews = Set<FullReview>()
                        appsWithNewReviews.forEach{ (_, newReviews) in
                            allReviews = allReviews.union(newReviews)
                        }
                        
                        EventLogger.logEvent(named: "finished_background_fetch", parameters: [
                            "number_of_apps" : appsWithNewReviews.count,
                            "number_of_reviews" : allReviews.count,
                            "time" : secondsElapsed
                            ])
                        
                        if allReviews.isEmpty {
                            completionHandler(UIBackgroundFetchResult.noData)
                        } else {
                            completionHandler(UIBackgroundFetchResult.newData)
                        }
                        endTimer()
                        
                    case .failure(let error):
                        log("Background fetch failed with error: \(error)")
                        completionHandler(UIBackgroundFetchResult.failed)
                        endTimer()
                    }
                    
                }
                
            default:
                completionHandler(UIBackgroundFetchResult.noData)
                endTimer()
            }
        }
        

    }
    
    func presentNotification(forApp app: App, withReview fullReview: FullReview) {
        
        let center = UNUserNotificationCenter.current()
        
        let trigger = UNTimeIntervalNotificationTrigger(timeInterval: 1, repeats: false)
        
        let notificationBuilder = NotificationBuilder()
        let content = notificationBuilder.notificationInfo(forApp: app, fullReview: fullReview)
        
        let request = UNNotificationRequest(identifier: UUID().uuidString, content: content, trigger: trigger)
        
        center.add(request)
    }
    
    func presentDebugNotification(withMessage message: String) {
        
        guard FIRRemoteConfig.remoteConfig().isAppInDebugMode else { return }
        
        let center = UNUserNotificationCenter.current()
        
        let trigger = UNTimeIntervalNotificationTrigger(timeInterval: 1, repeats: false)
        
        let content = UNMutableNotificationContent()
        content.title = "DEBUG"
        content.body = message
        
        let request = UNNotificationRequest(identifier: UUID().uuidString, content: content, trigger: trigger)
        
        center.add(request)
    }
    
    func configureDependencies() {
        
        Fabric.with([Crashlytics.self])
        FIRApp.configure()
        
        guard FIRRemoteConfig.remoteConfig().isAppInDebugMode else { return }
        
        DDLog.add(DDTTYLogger.sharedInstance()) // TTY = Xcode console
        DDLog.add(DDASLLogger.sharedInstance()) // ASL = Apple System Logs
        
        let fileLogger: DDFileLogger = DDFileLogger() // File Logger
        fileLogger.rollingFrequency = TimeInterval(60*60*24)  // 24 hours
        fileLogger.logFileManager.maximumNumberOfLogFiles = 30
        fileLogger.logFileManager.logFilesDiskQuota = 100
        fileLogger.maximumFileSize = 0
        DDLog.add(fileLogger)
    }
    
    func startBackgroundTask() {
        self.backgroundTaskIdentifier = UIApplication.shared.beginBackgroundTask(withName: "Update Reviews", expirationHandler: { [weak self] in
            guard let backgroundTaskIdentifier = self?.backgroundTaskIdentifier else { return }
            UIApplication.shared.endBackgroundTask(backgroundTaskIdentifier)
            self?.backgroundTaskIdentifier = UIBackgroundTaskInvalid
        })
    }
    
    func endBackgroundTask() {
        guard let backgroundTaskIdentifier = backgroundTaskIdentifier else { return }
        UIApplication.shared.endBackgroundTask(backgroundTaskIdentifier)
    }
    
    func startSubscriptionBackgroundTask() {
        self.subscriptionBackgroundTaskIdentifier = UIApplication.shared.beginBackgroundTask(withName: "Check Subscription", expirationHandler: { [weak self] in
            guard let backgroundTaskIdentifier = self?.subscriptionBackgroundTaskIdentifier else { return }
            UIApplication.shared.endBackgroundTask(backgroundTaskIdentifier)
            self?.subscriptionBackgroundTaskIdentifier = UIBackgroundTaskInvalid
        })
    }
    
    func endSubscriptionBackgroundTask() {
        guard let backgroundTaskIdentifier = subscriptionBackgroundTaskIdentifier else { return }
        UIApplication.shared.endBackgroundTask(backgroundTaskIdentifier)
    }
}


extension AppDelegate: UNUserNotificationCenterDelegate {
    
    func userNotificationCenter(_ center: UNUserNotificationCenter, willPresent notification: UNNotification, withCompletionHandler completionHandler: @escaping (UNNotificationPresentationOptions) -> Swift.Void) {
        
    }
        
    func userNotificationCenter(_ center: UNUserNotificationCenter, didReceive response: UNNotificationResponse, withCompletionHandler completionHandler: @escaping () -> Swift.Void) {
        
        let isShare = (response.actionIdentifier == shareAction.identifier)
        
        EventLogger.logEvent(named: "selected_notification", parameters: [
            "is_share" : isShare
            ])
        
        let reviewBuilder = ReviewBuilder()
        if let review = reviewBuilder.newReviewConstructedFrom(notification: response.notification) {
            
            guard let splitViewController = window?.rootViewController as? SplitViewController else {
                fatalError("Unexpected view heirachy")
            }
            
            splitViewController.display(review: review, toShare: isShare)
        }
    
    }
    
    
}

