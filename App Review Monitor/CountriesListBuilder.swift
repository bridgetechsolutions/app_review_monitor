//
//  CountriesListBuilder.swift
//  App Review Monitor
//
//  Created by Mark Bridges on 12/12/2016.
//  Copyright © 2016 Mark Bridges. All rights reserved.
//

import Foundation


class CountriesListBuilder {
    
    
    lazy private(set)var allCountries: Set<Country> = {
        
        let path = Bundle.main.path(forResource: "App Store Countries", ofType: "plist")!
        let arrayFromBundle = NSArray(contentsOfFile: path)
        
        let countryDictionaries = arrayFromBundle as! [[String:String]]
        
        var countries = Set<Country>()
        countryDictionaries.forEach{ dictionary in
            countries.insert(Country(id: dictionary["id"]!, name: dictionary["name"]!))
        }
        
        return countries
    } ()
    
    
    func country(forID id: String) -> Country? {
        return allCountries.filter{$0.id == id}.first
    }
    
    func countries(forIDs ids: [String]) -> Set<Country> {
        return Set(allCountries.filter{ ids.contains($0.id) })
    }
    
    func currentCountry() -> Country {
        
        let locale = Locale.current
        
        guard
            let regionCode = locale.regionCode,
            let currentCountry = country(forID: regionCode) else {
                return country(forID: "US")!
        }
    
        return currentCountry
    }
    
}
