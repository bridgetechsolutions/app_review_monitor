//
//  UIAlertController+ErrorHelpers.swift
//  NetAborter
//
//  Created by Mark Bridges on 24/11/2016.
//  Copyright © 2016 BridgeTech. All rights reserved.
//

import UIKit

extension UIAlertController {

    convenience init(title: String?, message: String?, defaultActionTitle: String? = L10n.globalOk.string, dismissedHandler: ((Void) -> Void)? = nil) {
        
        EventLogger.logEvent(named: "error_displayed", parameters: [
            "title" : title ?? "",
            "message" : message ?? ""
            ])

        self.init(title: title, message: message, preferredStyle: .alert)

        addAction(UIAlertAction(title: defaultActionTitle, style: .default, handler: { action in
            dismissedHandler?()
        }))

    }
    
    
    convenience init(error: Error, title: String? = nil, dismissedHandler: ((Void) -> Void)? = nil) {
        
        let title = title ?? L10n.errorAlertGenericTitle.string
        let message = error.localizedDescription
        
        self.init(title: title, message: message, dismissedHandler: dismissedHandler)
    }
    
}
