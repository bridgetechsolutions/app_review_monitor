//
//  DictionaryRepresentableProtocol.swift
//  App Review Monitor
//
//  Created by Mark Bridges on 19/12/2016.
//  Copyright © 2016 Mark Bridges. All rights reserved.
//

import Foundation


protocol DictionaryRepresentable {
    
    init?(dictionaryRepresentation : [String : Any])
    
    var dictionaryRepresentation: [String : Any] { get }
}


