//
//  AppsViewController.swift
//  App Review Monitor
//
//  Created by Mark Bridges on 13/12/2016.
//  Copyright © 2016 Mark Bridges. All rights reserved.
//

import UIKit
import CloudKit
import AlamofireImage
import MBProgressHUD


class AppsViewController: UIViewController {
    
    enum SelectingAppError: Error {
        case unableToFindCorrespondingApp
    }
    
    @IBOutlet private var emptyPlaceholderView: UIView?
    @IBOutlet private var emptyPlaceholderTitleLabel: UILabel? {
        didSet {
            emptyPlaceholderTitleLabel?.text = L10n.appsEmptyStateTitle.string
        }
    }
    @IBOutlet private var emptyPlaceholderDetailLabel: UILabel? {
        didSet {
            emptyPlaceholderDetailLabel?.text = L10n.appsEmptyStateMessage.string
        }
    }
    
    fileprivate let appsFetcher = AppsFetcher()
    fileprivate var sortedResults: [App] = []
    
    fileprivate lazy var refreshControl: UIRefreshControl = {
        let newRefreshControl = UIRefreshControl()
        newRefreshControl.addTarget(self, action: #selector(reloadDataWithoutCompletion), for: .valueChanged)
        return newRefreshControl
    } ()
    
    @IBOutlet weak var tableView: UITableView! {
        didSet {
            tableView.register(UINib(nibName: AppTableViewCell.identifier, bundle: nil), forCellReuseIdentifier: AppTableViewCell.identifier)
            tableView.dataSource = self
            tableView.delegate = self
            tableView.refreshControl = refreshControl
        }
    }
    
    var isEmptyPlaceholderShowing: Bool {
        get {
            return emptyPlaceholderView?.alpha == 0
        } set {
            UIView.animate(withDuration: 0.3) { [weak self] in
                self?.emptyPlaceholderView?.alpha = newValue ? 1 : 0
                self?.tableView?.alpha = newValue ? 0 : 1
            }
        }
    }
    
    var selectedApp: App? {
        guard
            let selectedIndexPath = tableView.indexPathForSelectedRow else {
                return nil
        }

        return sortedResults[selectedIndexPath.row]
    }
    
    
    // MARK: Instance Lifecycle
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        navigationItem.title = L10n.appsTitle.string
    }
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        navigationItem.leftBarButtonItem = editButtonItem
        
        reloadData()
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        tableView.deselectAllCells()
    }
    
    
    override func setEditing(_ editing: Bool, animated: Bool) {
        super.setEditing(editing, animated: animated)
        tableView.setEditing(editing, animated: animated)
    }
    
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        guard
            let reviewsNavigationController = segue.destination as? UINavigationController,
            let reviewsViewController = reviewsNavigationController.childViewControllers.first as? ReviewsViewController else {
                return
        }
        
        reviewsViewController.app = selectedApp
    }
    
    
    // MARK: Data Refresh
    
    func reloadDataWithoutCompletion() {
        reloadData(withCompletion: nil)
    }
    
    func reloadData(withCompletion completion: ((GenericResult) -> (Void))? = nil) {
        
        if sortedResults.isEmpty && refreshControl.isRefreshing == false {
            tableView.isLoading = true
        }
        
        appsFetcher.fetchAllApps(with: .name, completionHandler: { [weak self] result in
            
            DispatchQueue.main.async { [weak self] in
                
                self?.tableView.isLoading = false
                self?.refreshControl.endRefreshing()
                
                switch result {
                case .success(let apps):
                    self?.sortedResults = apps
                    UIView.animate(withDuration: 0.2,
                                   animations: {
                                    self?.tableView.reloadData()
                    },
                                   completion: { _ in
                                    completion?(.success)
                    })
                    self?.updateEmptyPlaceholderVisibility()
                    
                case .failure(let error):
                    // If we're calling this with a completion, the completion handler can deal with showing the error
                    if let completion = completion {
                        completion(.failure(error: error))
                    } else {
                        let alertController = UIAlertController(error: error)
                        self?.present(alertController, animated: true, completion: nil)
                    }
                }
            }
        })
    }
    
    fileprivate func updateEmptyPlaceholderVisibility() {
        isEmptyPlaceholderShowing = sortedResults.isEmpty
    }
    
    
    func selectApp(withID ID: String, completionHandler: @escaping (GenericResult) -> (Void)) {
        
        reloadData { [weak self] result in
            switch result {
            case .success:
                guard
                    let matchingApps = self?.sortedResults.filter({ $0.id == ID }),
                    let matchingApp = matchingApps.first,
                    let index = self?.sortedResults.index(of: matchingApp) else {
                        completionHandler(.failure(error: SelectingAppError.unableToFindCorrespondingApp))
                        return
                }
                
                let indexPath = IndexPath(row: index, section: 0)
                self?.tableView.selectRow(at: indexPath, animated: true, scrollPosition: .middle)
                self?.tableView(self!.tableView, didSelectRowAt: indexPath)
                completionHandler(.success)
                
            case .failure(let error):
                completionHandler(.failure(error: error))
            }
        }
    }
    
    // MARK: IBActions
    
    @IBAction func addPressed(_ sender: Any) {
        let appsSearchViewController = AppsSearchViewController(style: .plain)
        appsSearchViewController.delegate = self
        navigationController?.pushViewController(appsSearchViewController, animated: true)
    }
    
}


extension AppsViewController: UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        guard let selectedApp = selectedApp else { return }
        
        EventLogger.logEvent(named: "selected_app", parameters: [
            "app_name" : selectedApp.name,
            "app_id" : selectedApp.id,
            "total_number_of_apps" : sortedResults.count
            ])
        
        performSegue(withIdentifier: "toReviews", sender: self)
    }
}


extension AppsViewController: UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return sortedResults.count
    }
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: AppTableViewCell.identifier, for: indexPath) as! AppTableViewCell
        
        let app = sortedResults[indexPath.row]
        
        cell.myTextLabel?.text = app.name
        
        let imageViewSize = cell.myImageView?.frame.size ?? .zero
        
        let filter: AspectScaledToFillSizeWithRoundedCornersFilter?
        switch app.store {
        case .iPhone, .iPad:
            filter = AspectScaledToFillSizeWithRoundedCornersFilter(
                size: imageViewSize,
                radius: imageViewSize.height * 0.12
            )
        case .mac:
            filter = nil

        }
        
        cell.myImageView?.af_setImage(
            withURL: app.iconImageURL,
            placeholderImage: nil,
            filter: filter
        )
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        return true
    }
    
    func tableView(_ tableView: UITableView, canMoveRowAt indexPath: IndexPath) -> Bool {
        return false
    }
    
    func tableView(_ tableView: UITableView, shouldIndentWhileEditingRowAt indexPath: IndexPath) -> Bool {
        return true
    }
    
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        
        let app = sortedResults[indexPath.row]
        
        let progressHUD = MBProgressHUD.showAdded(to: view.window!, animated: true)
        progressHUD.removeFromSuperViewOnHide = true
        
        EventLogger.logEvent(named: "deleted_app", parameters: [
            "app_name" : app.name,
            "app_id" : app.id
            ])
        
        app.deleteAndUnsubscribe { [weak self] result in
            
            DispatchQueue.main.async { [weak self] in
                
                progressHUD.hide(animated: true)
                
                switch result {
                case .success:
                    self?.sortedResults.remove(at: indexPath.row)
                    tableView.deleteRows(at: [indexPath], with: .automatic)
                    self?.updateEmptyPlaceholderVisibility()
                    
                case .failure(let error):
                    let alertController = UIAlertController(error: error)
                    self?.present(alertController, animated: true, completion: nil)
                }
            }
        }
    }
}


extension AppsViewController: AppsSearchViewControllerDelegate {
    
    func appsSearchViewController(_ appsSearchViewController: AppsSearchViewController, completedWithResult result: AppsSearchViewController.Result) {
        
        DispatchQueue.main.async { [weak self] in
            
            _ = self?.navigationController?.popViewController(animated: true)
            
            switch result {
            case .appSaved(let app):
                
                User.currentUser { result in
                    switch result {
                    case .success(let user):
                        if user.dateFirstAppAdded == nil {
                            let updatedUser = User(id: user.id, dateFirstAppAdded: Date())
                            updatedUser.save { result in
                                switch result {
                                case .success:
                                    log("Saved first app added date")
                                    
                                case .failure(let error):
                                    log("Failed to save user, with error: \(error)")
                                }
                            }
                        }
                        
                    case .failure(let error):
                        log("Failed to retrieve user, with error: \(error)")
                    }
                }
                
                EventLogger.logEvent(named: "added_app", parameters: [
                    "app_name" : app.name,
                    "app_id" : app.id
                    ])
                
                self?.sortedResults.append(app)
                self?.sortedResults.sort { $0.0.name < $0.1.name }
                
                guard let row = self?.sortedResults.index(of: app) else {
                    fatalError("Can't find index path of inserted app")
                }
                
                let indexPath = IndexPath(row: row, section: 0)
                self?.tableView.insertRows(at: [indexPath], with: .automatic)
                self?.updateEmptyPlaceholderVisibility()
                
            default:
                break
            }
        }
    }
    
    
}
