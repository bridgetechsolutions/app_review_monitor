//
//  AppTableViewCell.swift
//  App Review Monitor
//
//  Created by Mark Bridges on 13/12/2016.
//  Copyright © 2016 Mark Bridges. All rights reserved.
//

import UIKit

class AppTableViewCell: UITableViewCell {
    
    static let identifier = "AppTableViewCell"
    
    @IBOutlet var myImageView: UIImageView?
    @IBOutlet var myTextLabel: UILabel?

    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        let bgColorView = UIView()
        bgColorView.backgroundColor = UIColor.selectedCell
        selectedBackgroundView = bgColorView
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

    }
    
    
    override func prepareForReuse() {
        myImageView?.image = nil
        myTextLabel?.text = nil
    }
    
}
