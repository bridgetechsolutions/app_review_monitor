//
//  StoreSearchService.swift
//  App Review Monitor
//
//  Created by Mark Bridges on 12/12/2016.
//  Copyright © 2016 Mark Bridges. All rights reserved.
//

import Foundation
import SwiftyJSON


class StoreSearchService {
    
    
    // MARK: Error Enums
    
    enum StoreSearchServiceError: Error {
        case emptyResponse
        case undeserializableResponse
        case unexpectedResponse
    }
        
    
    // MARK: Result Enums
    
    enum SearchResult {
        case success(apps: [App])
        case failure(error: Error)
    }
    
    
    // MARK: Properties
    
    let httpClient: HTTPClient
    private var previousSearchTask: URLSessionTask?
    
    // MARK: Initialisation
    
    required init(httpClient: HTTPClient = HTTPClient()) {
        self.httpClient = httpClient
    }
    
    
    // MARK: Network calls
    
    func getApps(forSearchTerm searchTerm: String, countryId: String = "us", store: App.Store, completion: @escaping ((SearchResult) -> Void)) {
        
        guard
            var urlComponents = URLComponents(string: "https://itunes.apple.com/\(countryId)/search?") else {
                fatalError("Unable to form URL for request")
        }
        
        let entityIdentifier: String
        switch store {
        case .iPhone:
            entityIdentifier = "software"
            
        case .iPad:
            entityIdentifier = "iPadSoftware"
            
        case .mac:
            entityIdentifier = "macSoftware"
        }

        let termQueryItem = URLQueryItem(name: "term", value: searchTerm)
        let entityQueryItem = URLQueryItem(name: "entity", value: entityIdentifier)
        let mediaQueryItem = URLQueryItem(name: "media", value: "software")
        
        urlComponents.queryItems = [termQueryItem, entityQueryItem, mediaQueryItem]

        guard
            let url = urlComponents.url else {
                fatalError("Unable to form URL for request")
        }
        
        previousSearchTask?.cancel()
        
        previousSearchTask = httpClient.makeNetworkRequest(with: url, completion: { result in
            
            switch result {
            case .success(let data):
                
                guard data.count > 0 else {
                    completion(.failure(error: StoreSearchServiceError.emptyResponse))
                    return
                }
                
                let json = JSON(data: data)
                
                guard
                    let results = json["results"].array else {
                        completion(.failure(error: StoreSearchServiceError.unexpectedResponse))
                        return
                }
                
                let apps: [App] = results.flatMap{ appJSON in
                    
                    let id = appJSON["trackId"].stringValue
                    
                    guard
                        let name = appJSON["trackCensoredName"].string,
                        let iconImageURL = appJSON["artworkUrl512"].url else {
                            return nil
                    }
                    
                    let countriesBuilder = CountriesListBuilder()
                    
                    return App(id: id, name: name, iconImageURL: iconImageURL, store: store, countries: countriesBuilder.allCountries, subscriptionID: nil, lastReviewsUpdate: nil)
                }
                
                completion(.success(apps: apps))
            
                
            case .failure(let error):
                
                // Don't return a failure for cancelled tasks
                let error = error as NSError
                guard
                    error.domain != "NSURLErrorDomain" && error.code != -999 else {
                        return
                }
                
                
                completion(.failure(error: error))
            }
            
        })
    }
}
