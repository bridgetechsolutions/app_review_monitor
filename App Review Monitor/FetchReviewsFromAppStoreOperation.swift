//
//  FetchReviewsFromAppStoreOperation.swift
//  App Review Monitor
//
//  Created by Mark Bridges on 11/12/2016.
//  Copyright © 2016 Mark Bridges. All rights reserved.
//

import Foundation
import SwiftyJSON


class FetchReviewsFromAppStoreOperation: AsynchronousOperation {
    
    enum FetchReviewsFromAppStoreOperation: Error {
        case unexpectedResponse
    }
    
    let app: App
    let isPaginationEnabled: Bool
    /// How far back we should look for new reviews, nil means all the way
    let endDate: Date?
    let country: Country
    let httpClient: HTTPClient
    
    private let operationQueue = OperationQueue()
    private var currentPageNumber = 1
    private var allReviews: Set<FullReview> = []
    
    required init(app: App, isPaginationEnabled: Bool, endDate: Date?, country: Country, httpClient: HTTPClient = HTTPClient()) {
        self.app = app
        self.isPaginationEnabled = isPaginationEnabled
        self.endDate = endDate
        self.country = country
        self.httpClient = httpClient
    }
    
    
    override func start() {
        super.start()
        
        func startOperationForCurrentPageNumber() {
            
            let fetchReviewsOperation = FetchReviewsFromAppStorePageOperation(app: app, country: country, pageNumber: currentPageNumber, httpClient: httpClient)
            fetchReviewsOperation.completionBlock = { [weak self, fetchReviewsOperation] in
                
                guard let strongSelf = self else { return }
                
                switch fetchReviewsOperation.operationStatus {
                case .succeeded(let result):
                    guard let (newResults, areThereMorePages) = result as? (Set<FullReview>, Bool) else { fatalError("Wrong result type") }
                    strongSelf.allReviews = strongSelf.allReviews.union(newResults)
                    
                    let reachedEndDate: Bool
                    if let oldestReviewDate = strongSelf.oldestRetrievedReviewDate,
                        let endDate = strongSelf.endDate {
                        reachedEndDate = oldestReviewDate < endDate
                    } else {
                        reachedEndDate = false
                    }
                    
                    // If there are no new results we call complete, otherwise we try the next page
                    if newResults.isEmpty || areThereMorePages == false || reachedEndDate || strongSelf.isPaginationEnabled == false {
                        strongSelf.operationStatus = .succeeded(result: strongSelf.allReviews)
                    } else {
                        strongSelf.currentPageNumber = strongSelf.currentPageNumber.advanced(by: 1)
                        startOperationForCurrentPageNumber()
                    }
                    
                case .failed(let error):
                    strongSelf.operationStatus = .failed(error: error)
                    
                default:
                    fatalError("Completion block called with incorrect status \(fetchReviewsOperation.operationStatus)")
                }
            }
            
            operationQueue.addOperation(fetchReviewsOperation)
        }
        
        startOperationForCurrentPageNumber()
    }
    
    
    private var oldestRetrievedReviewDate: Date? {
        let oldestReview = allReviews.sorted { $0.0.date > $0.1.date }.first
        
        return oldestReview?.date
    }
    
}
