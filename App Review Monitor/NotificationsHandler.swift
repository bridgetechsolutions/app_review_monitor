//
//  NotificationsHandler.swift
//  App Review Monitor
//
//  Created by Mark Bridges on 17/12/2016.
//  Copyright © 2016 Mark Bridges. All rights reserved.
//

import CloudKit


class NotificationsHandler {
    
    enum Result {
        case success(notifications: [CKNotification])
        case failure(error: Error)
    }
    
    
    func newCloudNotifications(withCompletionHandler completionHandler: @escaping ((Result) -> (Void))) {
        
        var notifications = [CKNotification]()
        
        let operation = CKFetchNotificationChangesOperation()
        operation.notificationChangedBlock = { notification in
            notifications.append(notification)
        }
        operation.fetchNotificationChangesCompletionBlock = { [unowned operation] token, error in
            
            if operation.moreComing {
                log("NEED TO IMPLEMENT 'MORECOMING' METHOD")
            }
            
            if let error = error {
                completionHandler(.failure(error: error))
            } else {
                completionHandler(.success(notifications: notifications))
            }
        }
        CKContainer.default().add(operation)
    }
    
}
