//
//  StarRatingView.swift
//  App Review Monitor
//
//  Created by Mark Bridges on 14/12/2016.
//  Copyright © 2016 Mark Bridges. All rights reserved.
//

import UIKit
import PureLayout


class StarRatingView: UIView {
    
    
    var fillColor = UIColor.star
    
    static let maxRating: Float = 5.0

    
    // 0 to 5
    var rating: Float = 0 {
        didSet {
            guard rating >= 0, rating <= StarRatingView.maxRating else { fatalError("Unsupported rating \(rating)")}
            refresh()
        }
    }
    
    private lazy var starsStackView: StarsStackView = {
        let stackView = StarsStackView(numberOfStars: Int(maxRating), isFilled: true, fillColor: self.fillColor)
        stackView.translatesAutoresizingMaskIntoConstraints = false
        return stackView
    } ()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setup()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setup()
    }
    
    
    private func setup() {
        
        addSubview(starsStackView)
        starsStackView.autoPinEdgesToSuperviewEdges()
        
        refresh()
    }

    
    private func refresh() {
        for (index, view) in starsStackView.arrangedSubviews.enumerated() {
            (view as! StarView).isFilled = index < Int(rating)
        }
        
    }
    
    
    // MARK: Stars Stack View
    
    class StarsStackView: UIStackView {
        
        convenience init(numberOfStars: Int, isFilled: Bool, fillColor: UIColor) {
            
            var starViews: [StarView] = []
            while starViews.count < numberOfStars {
                starViews.append(StarView(frame: .zero, isFilled: isFilled, fillColor: fillColor))
            }
            
            self.init(arrangedSubviews: starViews)
            
            self.axis = .horizontal
            self.distribution = .fillEqually
            self.alignment = .fill
            self.spacing = 2
            self.isOpaque = false
            self.backgroundColor = UIColor.clear
        }
    }
    
}

extension UIImage {
    class func image(withView view: UIView) -> UIImage {
        
        UIGraphicsBeginImageContextWithOptions(view.bounds.size, view.isOpaque, 0.0)
        view.layer.render(in: UIGraphicsGetCurrentContext()!)

//        view.drawHierarchy(in: view.bounds, afterScreenUpdates: true)
        let img = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        return img!
    }
}

//extension Float {
//    var isInteger: Bool {return rint(self) == self}
//}
