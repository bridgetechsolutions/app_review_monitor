//
//  Country.swift
//  App Review Monitor
//
//  Created by Mark Bridges on 10/12/2016.
//  Copyright © 2016 Mark Bridges. All rights reserved.
//

import Foundation

struct Country {
    
    let id: String
    let name: String
}


// MARK: Hashable

extension Country: Hashable {
    
    public var hashValue: Int {
        return id.hashValue
    }
    
}


// MARK: Equatable

extension Country: Equatable {
    
}


func ==(lhs: Country, rhs: Country) -> Bool {
    return lhs.hashValue == rhs.hashValue
}


// MARK: DictionaryRepresentation

extension Country: DictionaryRepresentable {
    
    init?(dictionaryRepresentation : [String : Any]) {
        
        guard
            let id = dictionaryRepresentation["id"] as? String,
            let name = dictionaryRepresentation["name"] as? String else {
                return nil
        }

        
        self.init(id: id, name: name)
    }
    
    var dictionaryRepresentation: [String : Any] {
        return [
            "id" : id,
            "name" : name
        ]
    }
}
