//
//  ReviewBuilder.swift
//  App Review Monitor
//
//  Created by Mark Bridges on 19/12/2016.
//  Copyright © 2016 Mark Bridges. All rights reserved.
//

import Foundation
import UserNotifications


class ReviewBuilder {
    
    
    func newReviewConstructedFrom(notification: UNNotification) -> Review? {
        
        let userInfo = notification.request.content.userInfo
        
        // First try to construct a full review
        if let reviewDictionary = userInfo["review"] as? [String : Any],
            let review = FullReview(dictionaryRepresentation: reviewDictionary) {
            return review
        }
        
        if let cloudKitDictionary = userInfo["ck"] as? [String : Any],
            let queryDictionary = cloudKitDictionary["qry"] as? [String : Any],
            let id = queryDictionary["rid"] as? String,
            let keysDictionary = queryDictionary["af"] as? [String : Any],
            let title = keysDictionary["title"] as? String,
            let content = keysDictionary["content"] as? String,
            let rating = keysDictionary["rating"] as? Float {
            
            let review = PartialReview(id: id, title: title, content: content, rating: rating)
            return review
        }
        
        
        return nil
    }
    
}
