//
//  User.swift
//  App Review Monitor
//
//  Created by Mark Bridges on 24/12/2016.
//  Copyright © 2016 Mark Bridges. All rights reserved.
//

import Foundation
import CloudKit


struct User {
    
    enum FetchCurrentUserResult {
        case success(user: User)
        case failure(error: Error)
    }
    
    let id: String
    let dateFirstAppAdded: Date?
    
    /// async gets iCloud record ID object of logged-in iCloud user
    static func currentUser(withCompletionHandler completionHandler: @escaping (FetchCurrentUserResult) -> (Void)) {
        
        let container = CKContainer.default()
        container.fetchUserRecordID() { recordID, error in
            if let error = error {
                completionHandler(.failure(error: error))
            } else {
                container.publicCloudDatabase.fetch(withRecordID: recordID!, completionHandler: { record, error in
                    
                    if let error = error {
                        completionHandler(.failure(error: error))
                    }
                    
                    guard
                        let record = record,
                        let user = User(record: record) else {
                            fatalError("Couldn't turn record into user")
                    }
                    
                    completionHandler(.success(user: user))
                })

            }
        }
    }
}


// MARK: CloudSyncable

extension User: CloudSyncable {
    
    init?(record: CKRecord) {
        self.id = record.recordID.recordName
        self.dateFirstAppAdded = record.object(forKey: "dateFirstAppAdded") as? Date
    }
    
    var record: CKRecord {
        let myRecord = CKRecord(recordType: User.recordName, recordID: recordID)
        myRecord.setObject(dateFirstAppAdded as CKRecordValue?, forKey: "dateFirstAppAdded")
        
        return myRecord
    }
    
    static var recordName: String {
        return "Users"
    }
    
    var recordID: CKRecordID {
        return CKRecordID(recordName: id)
    }
    
    static var database: CKDatabase {
        let container = CKContainer.default()
        return container.publicCloudDatabase
    }
    
}



