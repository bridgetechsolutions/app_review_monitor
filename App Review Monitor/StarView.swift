//
//  StarView.swift
//  App Review Monitor
//
//  Created by Mark Bridges on 14/12/2016.
//  Copyright © 2016 Mark Bridges. All rights reserved.
//

import UIKit


class StarView: UIView {
    
    var fillColor = UIColor.star
    
    var isFilled = false {
        didSet {
            setNeedsDisplay()
        }
    }
    
    convenience init(frame: CGRect, isFilled: Bool, fillColor: UIColor) {
        self.init(frame: frame)
        self.isFilled = isFilled
        self.fillColor = fillColor
        self.isOpaque = false
        self.translatesAutoresizingMaskIntoConstraints = false
        self.backgroundColor = UIColor.clear
    }
    
    override func draw(_ rect: CGRect) {
        super.draw(rect)
        
        // Get invalid contexts
        guard rect.size != .zero else { return }
        
        let frame = CGRect(x: 0, y: 0, width: rect.width, height: rect.height)
        
        if isFilled {
            //// FilledStar Drawing
            let filledStarPath = UIBezierPath()
            filledStarPath.move(to: CGPoint(x: frame.minX + 1.00000 * frame.width, y: frame.minY + 0.38196 * frame.height))
            filledStarPath.addLine(to: CGPoint(x: frame.minX + 0.62362 * frame.width, y: frame.minY + 0.37390 * frame.height))
            filledStarPath.addLine(to: CGPoint(x: frame.minX + 0.49999 * frame.width, y: frame.minY + 0.00000 * frame.height))
            filledStarPath.addLine(to: CGPoint(x: frame.minX + 0.37638 * frame.width, y: frame.minY + 0.37390 * frame.height))
            filledStarPath.addLine(to: CGPoint(x: frame.minX + 0.00000 * frame.width, y: frame.minY + 0.38196 * frame.height))
            filledStarPath.addLine(to: CGPoint(x: frame.minX + 0.30000 * frame.width, y: frame.minY + 0.62113 * frame.height))
            filledStarPath.addLine(to: CGPoint(x: frame.minX + 0.19098 * frame.width, y: frame.minY + 1.00000 * frame.height))
            filledStarPath.addLine(to: CGPoint(x: frame.minX + 0.49999 * frame.width, y: frame.minY + 0.77389 * frame.height))
            filledStarPath.addLine(to: CGPoint(x: frame.minX + 0.80903 * frame.width, y: frame.minY + 1.00000 * frame.height))
            filledStarPath.addLine(to: CGPoint(x: frame.minX + 0.69999 * frame.width, y: frame.minY + 0.62113 * frame.height))
            filledStarPath.addLine(to: CGPoint(x: frame.minX + 1.00000 * frame.width, y: frame.minY + 0.38196 * frame.height))
            filledStarPath.close()
            filledStarPath.miterLimit = 4;
            
            fillColor.setFill()
            filledStarPath.fill()
        } else {
            //// OutlinedStar Drawing
            let outlinedStarPath = UIBezierPath()
            outlinedStarPath.move(to: CGPoint(x: frame.minX + 1.00000 * frame.width, y: frame.minY + 0.38196 * frame.height))
            outlinedStarPath.addLine(to: CGPoint(x: frame.minX + 0.62362 * frame.width, y: frame.minY + 0.37390 * frame.height))
            outlinedStarPath.addLine(to: CGPoint(x: frame.minX + 0.49999 * frame.width, y: frame.minY + 0.00000 * frame.height))
            outlinedStarPath.addLine(to: CGPoint(x: frame.minX + 0.37638 * frame.width, y: frame.minY + 0.37390 * frame.height))
            outlinedStarPath.addLine(to: CGPoint(x: frame.minX + 0.00000 * frame.width, y: frame.minY + 0.38196 * frame.height))
            outlinedStarPath.addLine(to: CGPoint(x: frame.minX + 0.30000 * frame.width, y: frame.minY + 0.62113 * frame.height))
            outlinedStarPath.addLine(to: CGPoint(x: frame.minX + 0.19098 * frame.width, y: frame.minY + 1.00000 * frame.height))
            outlinedStarPath.addLine(to: CGPoint(x: frame.minX + 0.49999 * frame.width, y: frame.minY + 0.77389 * frame.height))
            outlinedStarPath.addLine(to: CGPoint(x: frame.minX + 0.80903 * frame.width, y: frame.minY + 1.00000 * frame.height))
            outlinedStarPath.addLine(to: CGPoint(x: frame.minX + 0.69999 * frame.width, y: frame.minY + 0.62113 * frame.height))
            outlinedStarPath.addLine(to: CGPoint(x: frame.minX + 1.00000 * frame.width, y: frame.minY + 0.38196 * frame.height))
            outlinedStarPath.close()
            outlinedStarPath.move(to: CGPoint(x: frame.minX + 0.70157 * frame.width, y: frame.minY + 0.84452 * frame.height))
            outlinedStarPath.addLine(to: CGPoint(x: frame.minX + 0.49999 * frame.width, y: frame.minY + 0.69702 * frame.height))
            outlinedStarPath.addLine(to: CGPoint(x: frame.minX + 0.29841 * frame.width, y: frame.minY + 0.84452 * frame.height))
            outlinedStarPath.addLine(to: CGPoint(x: frame.minX + 0.36953 * frame.width, y: frame.minY + 0.59737 * frame.height))
            outlinedStarPath.addLine(to: CGPoint(x: frame.minX + 0.17383 * frame.width, y: frame.minY + 0.44137 * frame.height))
            outlinedStarPath.addLine(to: CGPoint(x: frame.minX + 0.41936 * frame.width, y: frame.minY + 0.43610 * frame.height))
            outlinedStarPath.addLine(to: CGPoint(x: frame.minX + 0.50001 * frame.width, y: frame.minY + 0.19219 * frame.height))
            outlinedStarPath.addLine(to: CGPoint(x: frame.minX + 0.58064 * frame.width, y: frame.minY + 0.43610 * frame.height))
            outlinedStarPath.addLine(to: CGPoint(x: frame.minX + 0.82614 * frame.width, y: frame.minY + 0.44137 * frame.height))
            outlinedStarPath.addLine(to: CGPoint(x: frame.minX + 0.63044 * frame.width, y: frame.minY + 0.59737 * frame.height))
            outlinedStarPath.addLine(to: CGPoint(x: frame.minX + 0.70157 * frame.width, y: frame.minY + 0.84452 * frame.height))
            outlinedStarPath.close()
            outlinedStarPath.miterLimit = 4;
            
            fillColor.setFill()
            outlinedStarPath.fill()
        }
    }
}
